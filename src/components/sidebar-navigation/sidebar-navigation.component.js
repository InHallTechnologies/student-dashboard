import React, { useContext, useEffect, useState } from 'react';
import { MdSpaceDashboard, MdAddBox } from 'react-icons/md';
import { FaListAlt, FaUniversity, FaLink } from 'react-icons/fa';
import { MdWork } from 'react-icons/md'
import { AiFillCode } from 'react-icons/ai'
import { IoStatsChart, IoDocumentText } from 'react-icons/io5'
import './sidebar-navigation.style.scss';
import { useLocation, useNavigate } from 'react-router-dom';
import context from '../../context/app-context';
import logoSelected from "../../assets/sidebar_logo.png"
import logoUnSelected from "../../assets/sidebar_logo_grey.png"
import aiSelected from "../../assets/ai_selected.png"
import aiUnSelected from "../../assets/ai_unselected.png"

const SidebarNavigation = ({onChange}) => {

    const location = useLocation();
    const navigation = useNavigate()
    const [selectedTab, setSelectedTab] = useState("Dashboard");
    const [userData, setUserData] = useContext(context)
    const navigate = useNavigate()

    useEffect(() => {
        if (location.search){
            const params = new URLSearchParams(location.search);
            setSelectedTab(params.get('tab'));
        }
        
    } , [])

    const handleChange = option => {
        setSelectedTab(option)
        navigation(`/?tab=${option}`)
        if (onChange) {
            onChange(option)
        }
    }

    return(
        <div className='sidebar-navigation-container'>
            <div className={selectedTab === "Dashboard"?'selected-option-container':'option-container'} onClick={_ => handleChange("Dashboard")} >
                <MdSpaceDashboard size={18} color={selectedTab === "Dashboard"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Dashboard</h4>
            </div>
            {
                (userData.accountType === "VIA-COLLEGE" || userData.accountType === "LINKED")
                &&
                <div className={selectedTab === "DrivesByCollege"?'selected-option-container':'option-container'} onClick={_ => handleChange("DrivesByCollege")} >
                    <FaUniversity size={18} color={selectedTab === "DrivesByCollege"?"#3761EE": "#C4C4C4"} />
                    <h4 className='option-container-title' >Drives by College</h4>
                </div>
            }
            <div className={selectedTab === "DrivesByOccuHire"?'selected-option-container':'option-container'} onClick={_ => handleChange("DrivesByOccuHire")} >
                {/* <FaUniversity size={18} color={selectedTab === "DrivesByOccuHire"?"#3761EE": "#C4C4C4"} /> */}
                <img src={selectedTab==="DrivesByOccuHire"?logoSelected:logoUnSelected} alt="OccuHire" style={{width:23, height:23}} />
                <h4 className='option-container-title' >Drives by OccuHire</h4>
            </div>

            <div className={selectedTab === "PlacementStatus"?'selected-option-container':'option-container'} onClick={_ => handleChange("PlacementStatus")} >
                <MdWork size={18} color={selectedTab === "PlacementStatus"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Placement Status</h4>
            </div>

            <div className={selectedTab === "AssessmentResults"?'selected-option-container':'option-container'} onClick={_ => handleChange("AssessmentResults")} >
                <AiFillCode size={18} color={selectedTab === "AssessmentResults"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Assessment Results</h4>
            </div>

            <div className={selectedTab === "TakeAssessment"?'selected-option-container':'option-container'} onClick={_ => handleChange("TakeAssessment")} >
                <IoStatsChart size={18} color={selectedTab === "TakeAssessment"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Take Assessment</h4>
            </div>

            <div className={selectedTab === "PersonalizedResult"?'selected-option-container':'option-container'} onClick={_ => navigate("/upgrade-plan")} >
                <img src={selectedTab==="DrivesByOccuHire"?aiSelected:aiUnSelected} alt="OccuHire" style={{width:20, height:20}} />
                <h4 className='option-container-title' >AI Based Recommendation</h4>
            </div>

            {
                userData.accountType === "DIRECT"
                &&
                <div className={selectedTab === "LinkMyCollege"?'selected-option-container':'option-container'} onClick={_ => handleChange("LinkMyCollege")} >
                    <FaLink size={18} color={selectedTab === "LinkMyCollege"?"#3761EE": "#C4C4C4"} />
                    <h4 className='option-container-title' >Link my College</h4>
                </div>
            }

            <div className={selectedTab === "ManageResume"?'selected-option-container':'option-container'} onClick={_ => handleChange("ManageResume")} >
                <IoDocumentText size={18} color={selectedTab === "ManageResume"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Manage Resume</h4>
            </div>
        </div>
    )
}
export default SidebarNavigation;


//rgba(24, 119, 211, 255)