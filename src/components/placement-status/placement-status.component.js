import React, { useEffect, useState, useContext } from "react";
import Header from "../header/header.component";
import './placement-status.style.scss'
import context from '../../context/app-context';
import { BiSearchAlt } from 'react-icons/bi'
import { Table, Button } from "react-bootstrap";
import { get, child, ref } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";

const PlacementStatus = () => {

    const [drivesList, setDrivesLIst] = useState([])
    const [originalList, setOriginalList] = useState([])
    const [loading, setLoading] = useState(true)
    const [searchWord, setSearchWord] = useState("")
    const [userData, setUserData] = useContext(context)

    useEffect(() => {
        let temp = []
        console.log("UID: " + userData.uid)
        get(child(ref(firebaseDatabase), "APPLICATION_STATUS/"+userData.uid)).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    temp.push(snapShot.child(key).val())
                }
                setDrivesLIst(temp.reverse())
                setOriginalList(temp)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])

    const handleSearch = () => {
        let temp = []
        for (const index in originalList) {
            if (originalList[index].companyName.toLowerCase().includes(searchWord.toLowerCase()) || originalList[index].location.toLowerCase().includes(searchWord.toLowerCase()) || originalList[index].role.toLowerCase().includes(searchWord.toLowerCase())) {
                temp.push(originalList[index])
            }
        }
        setDrivesLIst(temp)
    }

    return(
        <div className="placement-status-container">
            <Header />

            <div className="search-container">
                <BiSearchAlt color="#ACACAC" size={18} />
                <input className="search-bar" placeholder="Company Name, Role or Location" type="text" value={searchWord} onKeyUp={(event)=>{if(event.key === 'Enter' || event.keyCode === 13){handleSearch()}}} onChange={(event)=>{setSearchWord(event.target.value)}} />
            </div>

            <div className="drives-list-container">
                <p className="section-title">Placement status of your applications</p>

                {
                    drivesList.length === 0
                    ?
                    <p className="no-drive">No updates here. Apply in a company now!</p>
                    :
                    <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>Company</th>
                            <th>Role</th>
                            <th>Package</th>
                            <th>Location</th>
                            <th>Offer</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            drivesList.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.companyName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.role}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.startPackage}L-{item.endPackage}L</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.location}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.status==="Placed"?"₹ "+(parseFloat(item.package)/100000).toFixed(1)+"L":"-"}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>
                                        <Button className="upgrade-button" style={item.status==="Placed"?{backgroundColor:"#4BCD7F"}:item.status==="Rejected"?{backgroundColor:"#D43B3B"}:item.status==="Applied"?{backgroundColor:"#52E1E2"}:{backgroundColor:"#FFB800"}} variant="primary">{item.status}</Button>
                                        {
                                            item.status==="Applied" && item.registrationLink !== ""
                                            &&
                                            <Button className="upgrade-button" variant="primary" onClick={()=>{window.open(item.registrationLink, "_blank")}} >Register</Button>
                                        }
                                    </div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                    
                </Table>  
                }
            </div>
        </div>
    )
}

export default PlacementStatus