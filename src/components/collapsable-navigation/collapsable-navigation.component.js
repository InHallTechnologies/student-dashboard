import React, { useState } from "react";
import "./collapsable-navigation.style.scss";
import { BiMenu } from "react-icons/bi";
import logo from "../../assets/logo.png";
import { Image, Offcanvas } from "react-bootstrap";
import SidebarNavigation from "../sidebar-navigation/sidebar-navigation.component";

const CollapsableNavigation = ({ onChange }) => {
    const [show, setShow] = useState(false);

    const openDrawer = (_) => setShow(true);
    const handleClose = (_) => setShow(false);

    const handleChange = (option, shouldCloseDrawer) => {
        if (shouldCloseDrawer === false) {
            onChange(option)
            return;
        }
        onChange(option)
        setShow(false);
    }
    

    return (
        <>
        <div className="collapsable-navigation-container">
            <BiMenu size={35} color="#444" onClick={openDrawer} />
            <Image className="mobile-logo" src={logo} />
        </div>
            <Offcanvas style={{maxWidth:"80%"}} show={show} onHide={handleClose}>
                <Offcanvas.Header closeButton>
                    <Offcanvas.Title>
                        <Image style={{ height:'70px' }}  src={logo} />
                    </Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                    <SidebarNavigation onChange={handleChange}  />
                </Offcanvas.Body>
            </Offcanvas>
        </>
        
    );
};

export default CollapsableNavigation;
