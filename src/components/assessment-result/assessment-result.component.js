import React, { useState, useContext, useEffect } from "react";
import context from "../../context/app-context";
import "./assessment-result.style.scss"
import { get, child, ref } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import Header from "../header/header.component";
import { Button, Table } from "react-bootstrap";

const AssessmentResult = () => {

    const [resultList, setResultList] = useState([])
    const [loading, setLoading] = useState(true)
    const [userData, setUserData] = useContext(context)

    useEffect(() => {
        let temp = []
        console.log("UID: " + userData.uid)
        get(child(ref(firebaseDatabase), "ASSESSMENT_RESULTS/"+userData.uid)).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    temp.push(snapShot.child(key).val())
                }
                setResultList(temp.reverse())
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])

    return(
        <div className="assessment-result-container">
            <Header />

            <div className="drives-list-container">
                <p className="section-title">Assessment results</p>

                {
                    resultList.length === 0
                    ?
                    <p className="no-drive">No results yet. Take an assessment now!</p>
                    :
                    <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>Type</th>
                            <th>Course Name</th>
                            <th>Max Score</th>
                            <th>Score Achieved</th>
                            <th>Date</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            resultList.map((item, index) => {return(
                                <tr>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.type==="PRE"?"Pre-qualifying":"Skill Assessment"}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.courseName}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.totalScore}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.scoreAchieved}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.date.split(" ")[0]}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" style={item.badge==="green"?{backgroundColor:"#4BCD7F"}:item.badge==="orange"?{backgroundColor:"#FFB800"}:{backgroundColor:"#D43B3B"}} variant="primary">{item.percentage}</Button></div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                    
                </Table>  
                }
            </div>
        </div>
        
    )
}

export default AssessmentResult