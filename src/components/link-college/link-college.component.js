import React, { useContext, useEffect, useState } from "react";
import context from "../../context/app-context";
import Header from "../header/header.component";
import './link-college.style.scss'
import { get, ref, child, remove, set } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import { Button, Form } from "react-bootstrap";
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from "react-router-dom";

const LinkCollege = () => {

    const [colleges, setCollege] = useState([])
    const [userData, setUserData] = useContext(context)
    const [processing, setProcessing] = useState(false)
    const [selectedCollege, setSelectedCollege] = useState({})

    const navigate = useNavigate()

    useEffect(()=>{
        var temp = []

        get(child(ref(firebaseDatabase), "COLLEGE_ARCHIVE")).then( async (snapShot)=>{
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    const collegeDetails = await snapShot.child(key).val()
                    temp.push(collegeDetails)
                }
                setCollege(temp)
                setSelectedCollege(temp[0])
            }
        })
    }, [])

    const handleVerify = () => {
        setProcessing(true)
        console.log(selectedCollege)
        get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+selectedCollege.uid+"/"+userData.department)).then( async (snapShot)=>{
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    var item = snapShot.child(key).val()
                    if (item.usn === userData.usn) {
                        userData.accountType = "LINKED"
                        userData.collegeKey = key
                        userData.collegeUid = selectedCollege.uid
                        userData.collegeName = selectedCollege.collegeName
                        await set(ref(firebaseDatabase, "STUDENTS_ARCHIVE/"+userData.department+"/"+userData.uid), userData)
                        await set(ref(firebaseDatabase, "COLLEGE_WISE_STUDENTS/"+selectedCollege.uid+"/"+userData.department+"/"+userData.collegeKey), userData)
                        setUserData(userData)
                        toast.success("College linked successfully.")
                        const timer = setTimeout(() => {
                            navigate(`/?tab=Dashboard`)
                            window.location.reload(false)
                            setProcessing(false)
                        }, 1500);
                        return () => clearTimeout(timer);
                    }
                }
                toast.error("Sorry, your records could not be found in college data.")
                setProcessing(false)
            } else {
                toast.error("Sorry, your records could not be found in college data.")
                setProcessing(false)
            }
        })
    }

    return(
        <div className="link-college-container">
            <Header />

            <div className="form-button-container">
                <Form className="link-form-container">
                    <p className="ete-field section-title">Confirm and submit the details</p>
                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1" style={{marginRight:10}}>
                        <Form.Label className="tag">Select you college</Form.Label>
                        <Form.Select aria-label="Default select example" onChange={(event)=>{setSelectedCollege(JSON.parse(event.target.value)); console.log(JSON.parse(event.target.value).collegeName)}}>
                            {
                                colleges.map((item, index)=>{return(
                                    <option className="field" value={JSON.stringify(item)}>{item.collegeName}</option>
                                )})
                                }
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Department</Form.Label>
                        <Form.Control required className="field" value={userData.department} disabled />
                    </Form.Group>
                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">USN</Form.Label>
                        <Form.Control required className="field" value={userData.usn} disabled />
                    </Form.Group>
                </Form>

                <Button onClick={handleVerify} variant="primary" className="primary" type="submit">Verify</Button>
            </div>
            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />
        </div>
    )
}

export default LinkCollege