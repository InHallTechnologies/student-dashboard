import React, { useContext, useEffect, useState } from "react";
import './take-assessment.style.scss'
import { get, child, ref, remove } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import Header from "../header/header.component";
import { Button, Spinner, Modal, ModalBody } from "react-bootstrap";
import alert from '../../assets/alert.png'
import { useNavigate } from "react-router";
import context from "../../context/app-context";

const TakeAssessment = () => {

    const [testList, setTestList] = useState([])
    const [preQuals, setPreQuals] = useState([])
    const [skills, setSkills] = useState([])
    const [loadingResults, setLoadingResults] = useState(true)
    const [assmntResults, setAssmntResults] = useState([])
    const [userData, setUserData] = useContext(context)
    const navigation = useNavigate()
    const [selectedTab, setSelectedTab] = useState("PRE")
    const [confirmModal, setConfirmModal] = useState(false)
    const [selectedTest, setSelectedTest] = useState({})

    useEffect(() => {
        var preQualTemp = []
        var skillTemp = []
        let tempResults = []
        get(child(ref(firebaseDatabase), "ASSESSMENT_DETAILS")).then((snapShot)=>{
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    skillTemp.push(snapShot.child(key).val())
                }
                setSkills(skillTemp.reverse())
            } 
        })
        get(child(ref(firebaseDatabase), "PREQUALIFYING_EXAMS")).then((snapShot)=>{
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    preQualTemp.push(snapShot.child(key).val())
                }
                setPreQuals(preQualTemp.reverse())
                setTestList(preQualTemp)
            } 
        })
        get(child(ref(firebaseDatabase), "ASSESSMENT_RESULTS/"+userData.uid)).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    tempResults.push(snapShot.child(key).val())
                }
                setAssmntResults(tempResults.reverse())
                setLoadingResults(false)
            } else {
                setLoadingResults(false)
            }
        })
    }, [])
    
    const handleTest = async () => {
        for (const index in assmntResults) {
            if (assmntResults[index].id === selectedTest.id) {
                const date1 = new Date(assmntResults[index].date.split(" ")[0])
                var date = new Date().getDate(); 
                var month = new Date().getMonth() + 1; 
                var year = new Date().getFullYear(); 
                const date2 = new Date(month + "/" + date + "/" + year)
                const diffTime = Math.abs(date2 - date1);
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                console.log(diffDays)
                if (diffDays <= 15) {
                    alert("Sorry, you took this assessment less than 15 days ago. You will have to wait for " + (15-diffDays) + " days before you can take this test again.")
                    return
                }
                await remove(ref(firebaseDatabase, "ASSESSMENT_RESULTS/"+userData.uid+"/"+selectedTest.id))
                navigation('/test',{state:{item:selectedTest, type:selectedTab}})
                return
            }
        }
        await remove(ref(firebaseDatabase, "ASSESSMENT_RESULTS/"+userData.uid+"/"+selectedTest.id))
        navigation('/test',{state:{item:selectedTest, type:selectedTab}})
    }

    return(
        <div className="take-assessment-container">
            <Header />

            <div className="title-list-container">

                <div className="tab-container">
                    <Button variant="primary" className={selectedTab==="PRE"?"selected":"unselected"} onClick={()=>{setSelectedTab("PRE"); setTestList(preQuals)}}>Pre-qualifying Exams</Button>
                    <Button variant="primary" className={selectedTab==="SKILL"?"selected":"unselected"} onClick={()=>{setSelectedTab("SKILL"); setTestList(skills)}}>Skill Assessments</Button>
                </div>
                <p className="section-title">Select a {selectedTab==="PRE"?"Pre-Qualifying Exam":"Skill Assessment"} you wish to take</p>

                <div className="course-list-container">
                    {
                        (loadingResults)
                        ?
                        <Spinner animation="border" variant="primary" style={{marginTop:50, alignSelf:"center"}} />
                        :
                        testList.length === 0
                        ?
                        <p style={{marginTop:50, alignSelf:"center", fontWeight:600, fontSize:16, color:"#ACACAC"}}>No assessments added yet!</p>
                        :
                        testList.map((item, index)=>{return(
                            <div onClick={()=>{setConfirmModal(true); setSelectedTest(item)}} className="course-container" >
                                <div className="logo-container">
                                    <img className="course-logo" src={item.courseLogo} alt="course-icon"  />
                                </div>
                                <p className="course-name">{item.courseName}</p>
                            </div>
                        )})
                    }
                </div>
            </div>
            
            <Modal show={confirmModal} onHide={()=>{setConfirmModal(false)}}>
                <Modal.Header closeButton>
                <Modal.Title>Take test?</Modal.Title>
                </Modal.Header>
                <Modal.Body>Please read the below instructions carefully.</Modal.Body>
                <ModalBody>
                    <div>
                        <div style={{display:"flex", flexDirection:"row", marginBottom:10}}>
                            <img src={alert} style={{width:12, height:12, marginRight:5}} />
                            <p style={{margin:0, padding:0, marginTop:'-5px'}}>Clicking on "Continue" will begin the test.</p>
                        </div>
                        <div style={{display:"flex", flexDirection:"row", marginBottom:10}}>
                            <img src={alert} style={{width:12, height:12, marginRight:5}} />
                            <p style={{margin:0, padding:0, marginTop:'-5px'}}>Hitting back at any moment before submitting will auto-submit the test.</p>
                        </div>
                        <div style={{display:"flex", flexDirection:"row", marginBottom:10}}>
                            <img src={alert} style={{width:12, height:12, marginRight:5}} />
                            <p style={{margin:0, padding:0, marginTop:'-5px'}}>Refreshing the page at any moment before submitting will auto-submit the test.</p>
                        </div>
                    </div>
                </ModalBody>
                <Modal.Footer>
                <Button variant="secondary" onClick={()=>{setConfirmModal(false)}}>
                    Cancel
                </Button>
                <Button variant="primary" onClick={()=>{handleTest(); setConfirmModal(false)}}>
                    Continue
                </Button>
                </Modal.Footer>
            </Modal>

        </div>
    )
}

export default TakeAssessment