import React from "react";
import "./Footer.style.scss";
import { AiFillLinkedin, AiFillTwitterSquare, AiFillFacebook } from 'react-icons/ai'
import { HiMail } from 'react-icons/hi'
import { IoCall } from 'react-icons/io5'

const FooterContainer = () => {
    return (
        <div className="footer-container">
            <div className="footer-content">
                <div className="top-container">
                    <div className="footer-data" >
                        <h2 className="footer-data-title">Address</h2>
                        <p className="footer-data-para">No. 10, 3rd A Cross</p>
                        <p>Anjaneya Nagar, BSK 3rd Stage</p>
                        <p>Bengaluru - 560085</p>
                    </div>

                    <div className="footer-data" >
                        <h2 className="footer-data-title">Reach Us</h2>
                        <div className="footer-data-list">
                            <div className="footer-data-list-item" >
                                <HiMail size={25} color='#fff' />
                                <p className="footer-data-list-item-name">queries@occuhire.com</p>
                            </div>

                            <div className="footer-data-list-item" >
                                <IoCall size={25} color='#fff' />
                                <p className="footer-data-list-item-name">+91 80880 95698</p>
                            </div>

                            <div className="footer-data-list-item" >
                                <IoCall size={25} color='#fff' />
                                <p className="footer-data-list-item-name">+91 80882 19317</p>
                            </div>
                        </div>
                    </div>

                    <div className="footer-data" >
                        <h2 className="footer-data-title">Follow Us</h2>
                        <div className="footer-data-list">
                            <div className="footer-data-list-item" >
                                <AiFillLinkedin size={25} color='#fff' />
                                <p className="footer-data-list-item-name">LinkedIn</p>
                            </div>

                            <div className="footer-data-list-item" >
                                <AiFillTwitterSquare size={25} color='#fff' />
                                <p className="footer-data-list-item-name">Twitter</p>
                            </div>

                            <div className="footer-data-list-item" >
                                <AiFillFacebook size={25} color='#fff' />
                                <p className="footer-data-list-item-name">Facebook</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="bottom-container">
                    Copyright © 2021 Tequed Labs Private Limited
                </div>
            </div>
        </div>
    );
};

export default FooterContainer;
