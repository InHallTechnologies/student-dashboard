import React from 'react';
import './HowToLandJob.style.scss';
import businessman from '../../assets/businessman.png';

const HowToLandJob = () => {

    return (
        <div className='how-to-land-job-container' >
            <div className='land-job-illustration-container' >
                <h2 className='section-title' >5 STEPS TO LAND IN YOUR DREAM JOB</h2>
                <img className='land-dream-job'  alt='Five Steps To Land in your dream job' src={businessman} />
            </div>

            <div className='land-job-content' >
                <div className='steps-container'>
                    <div className='steps' >
                        <div className='steps-index' >1</div>
                        <p className='steps-info'>Sign up for the OccuHire Portal and fill in all your information.</p>
                    </div>

                    <div className='steps' >
                        <div className='steps-index' >2</div>
                        <p className='steps-info'>Have a personalised mentorship session with our expert mentor.</p>
                    </div>


                    <div className='steps' >
                        <div className='steps-index' >3</div>
                        <p className='steps-info'>Take company specific pre-qualification test to increase your chances of getting hired.</p>
                    </div>


                    <div className='steps' >
                        <div className='steps-index' >4</div>
                        <p className='steps-info'>Optimise your resume, reskill yourself and apply for 10K+ job roles of your choice.</p>
                    </div>


                    <div className='steps' >
                        <div className='steps-index' >5</div>
                        <p className='steps-info'>Get hired and start working.</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HowToLandJob;