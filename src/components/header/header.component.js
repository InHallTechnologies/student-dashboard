import React, { useContext, useState, useEffect} from "react";
import './header.style.scss'
import context from '../../context/app-context';
import { Button, Dropdown, Image, Spinner } from 'react-bootstrap';
import { get, ref, child, set } from 'firebase/database'
import { firebaseAuth, firebaseDatabase, firebaseStorage } from "../../backend/firebase-handler";
import { ref as firebaseStorageRef, uploadBytesResumable, getDownloadURL } from 'firebase/storage';
import { Router, useNavigate } from "react-router-dom";

const Header = () => {

    const [userData, setUserData] = useContext(context)
    const [planDetails, setPlanDetails] = useState({})
    const [uploadingImage, setUploadingImage] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        get(child(ref(firebaseDatabase), "STUDENT_PLAN_DETAILS/"+userData.plan)).then((snapshot) => {
            if (snapshot.exists()) {
                setPlanDetails(snapshot.val())
            }
        })   
    }, [])

    const handleImage = () => {
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type',"file");
        inputElement.setAttribute('accept','image/*');
        inputElement.onchange = (event) => {
            const files = event.target.files;
            const logoRef = firebaseStorageRef(firebaseStorage, `STUDENT_PICTURE/`+userData.uid);
            const uploadTask = uploadBytesResumable(logoRef,files[0], {contentType:'image/png'}); 
            uploadTask.on('state_changed', (snapshot) => {
                setUploadingImage(true);
            }, () => {
                alert("Something went wrong.")
                setUploadingImage(false);
            }, async () => {
                const url = await getDownloadURL(logoRef);
                console.log(url)
                setUserData({...userData, picture:url});
                await set(ref(firebaseDatabase, "STUDENTS_ARCHIVE/"+userData.department+"/"+userData.uid+"/picture"), url)
                if (userData.accountType === "LINKED") {
                    await set(ref(firebaseDatabase, "COLLEGE_WISE_STUDENTS/"+userData.collegeUid+"/"+userData.department+"/"+userData.collegeKey+"/picture"), url)
                }
                setUploadingImage(false);
            })

        }
        inputElement.click();
    }

    return(
        <div className="header-container">
            <div className="name-plan-container">
                <p className="name">{userData.name}</p>
                <div className="plan-upgrade-container">
                    <p className="plan">{userData.plan}</p>
                    <Button onClick={_ => navigate('/upgrade-plan')} style={planDetails.upgradable?{}:{display:"none"}} variant="primary" className="upgrade-button">Upgrade</Button>
                </div>
            </div>
            
            <Dropdown className="dropdown-image-container">
                <Dropdown.Toggle as='image' className="image-container" >
                    {
                        uploadingImage
                        ?
                        <Spinner color="#3761EE" size="small" />
                        :
                        <Image id="dropdown-basic" roundedCircle className="profile-picture" style={{width:80, height:80}} src={userData.picture?userData.picture:'https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde'} />
                    }
                </Dropdown.Toggle>

                <Dropdown.Menu>
                    <Dropdown.Item onClick={()=>{navigate("manage-profile")}}>Manage profile</Dropdown.Item>
                    <Dropdown.Item onClick={()=>{navigate("/");firebaseAuth.signOut()}} >Logout</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
    )
}

export default Header