import { child, get, ref } from 'firebase/database'
import React, { useEffect, useState, useRef } from 'react'
import { Carousel } from 'react-bootstrap'
import { firebaseDatabase } from '../../backend/firebase-handler'
import "./carousel-container.style.scss"

const CarouselContainer = () => {

    const [imageList, setImageList] = useState([])
    const [caroselWidth, setCaroselWidth] = useState(0);
    const caroselContainerRef = useRef()

    useEffect(()=>{
        var temp = []
        setCaroselWidth(caroselContainerRef.current.offsetWidth);
        window.addEventListener('resize', setCaroselWidthHandler)
        get(child(ref(firebaseDatabase), "CAROUSELS/STUDENT_DASHBOARD")).then((snap) => {
            if (snap.exists()) {
                temp = snap.val()
            }
            setImageList(temp)
        })
    }, [])

    const setCaroselWidthHandler = () => {
        if (caroselContainerRef.current) {
            setCaroselWidth(caroselContainerRef.current.offsetWidth);
        }
    }

    return(
        <div className='carousel-container' ref={caroselContainerRef} >
            <Carousel>
                {
                    imageList.map((item, index) => {return(
                        <Carousel.Item className="image-container" interval={3000}>
                            <img className="image" style={{width: `${caroselWidth}px`}} src={item} alt="OccuHire-Student" />
                        </Carousel.Item>
                    )})
                }
            </Carousel>
        </div>
    )
}

export default CarouselContainer