import React from 'react';
import './SimpleGridWithTitle.style.scss';

const SimpleGridWithTitle = ({ title, list }) => {


    return (
        <div className='simple-grid-with-list'>
            <h2 className='grid-with-title-title'>{title}</h2>
            <div className='grid-list-container' >
                {
                    list.map(item => <img key={item} className='list-image' src={item} />)
                }
            </div>
        </div>
    )
}

export default SimpleGridWithTitle;