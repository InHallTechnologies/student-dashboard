import React, { useEffect, useContext, useState } from "react";
import './manage-resume.style.scss'
import context from '../../context/app-context';
import { Image, Button, Table, Form, Spinner, Dropdown } from 'react-bootstrap';
import { RiAccountCircleFill } from 'react-icons/ri'
import { get, ref, child, set } from 'firebase/database'
import { firebaseAuth, firebaseDatabase, firebaseStorage } from "../../backend/firebase-handler";
import { FiDownload } from 'react-icons/fi'
import 'react-toastify/dist/ReactToastify.css';
import { toast, ToastContainer } from "react-toastify";
import departmentList from "../../entities/department-list";
import { ref as firebaseStorageRef, uploadBytesResumable, getDownloadURL } from 'firebase/storage';
import Header from "../header/header.component";
import axios from 'axios';

const ManageResume = () => {

    const [userData, setUserData] = useContext(context)
    const [userProfile, setUserProfile] = useState(userData?userData:{})
    const [uploadingImage, setUploadingImage] = useState(false);
    const [planDetails, setPlanDetails] = useState({})
    const [contentLoading, setContentLoading] = useState(true)
    const [uploading, setUploading] = useState(false)
    const [tempName, setTempName] = useState("")
    const [tempDesc, setTempDesc] = useState("")
    const [tempIntComp, setTempComp] = useState("")
    const [tempDuration, setTeampDUration] = useState("")
    const [tempRole, setTempRole] = useState("")
    const [tempStartDate, setTempStartDate] = useState("")
    const [tempEndDate, setTempEndDate] = useState("")
    const [tempIntDisc, setTempIntDisc] = useState("")
    const [certName, setCertName] = useState("")
    const [certAgency, setCertAgency] = useState("")
    const [certDate, setCertDate] = useState("")
    const [paperTitle, setPaperTitle] = useState("")
    const [paperAgency, setPaperAgency] = useState("")
    const [paperDate, setPaperDate] = useState("")
    const [paperdisc, setPaperdisc] = useState("")
    const [awardName, setAwardName] = useState("")
    const [awardDate, setAwardDate] = useState("")
    const [awardDisc, setAwardDisc] = useState("")
    const [addDetail, setAddDetail] = useState("")
    const [addName, setAddName] = useState("")

    const [tempArray, setTempArray] = useState(["a", "c"])

    useEffect(() => {
        get(child(ref(firebaseDatabase), "STUDENT_PLAN_DETAILS/"+userData.plan)).then((snapshot) => {
            if (snapshot.exists()) {
                setPlanDetails(snapshot.val())
            }
        })  
        console.log(userData)
        setUserProfile(userData)
        setContentLoading(false)
    }, [])

    const handleImage = () => {
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type',"file");
        inputElement.setAttribute('accept','image/*');
        inputElement.onchange = (event) => {
            const files = event.target.files;
            const logoRef = firebaseStorageRef(firebaseStorage, `STUDENT_PICTURE/`+userData.uid);
            const uploadTask = uploadBytesResumable(logoRef,files[0], {contentType:'image/png'}); 
            uploadTask.on('state_changed', (snapshot) => {
                setUploadingImage(true);
            }, () => {
                alert("Something went wrong.")
                setUploadingImage(false);
            }, async () => {
                const url = await getDownloadURL(logoRef);
                console.log(url)
                setUserData({...userData, picture:url});
                await set(ref(firebaseDatabase, "STUDENTS_ARCHIVE/"+userData.department+"/"+userData.uid+"/picture"), url)
                if (userProfile.accountType === "LINKED") {
                    await set(ref(firebaseDatabase, "COLLEGE_WISE_STUDENTS/"+userData.collegeUid+"/"+userData.department+"/"+userData.collegeKey+"/picture"), url)
                }
                setUploadingImage(false);
            })

        }
        inputElement.click();
    }

    const handleAdd = (type) => {
        switch(type) {
            case "project": {
                var tempObj = {name:tempName, description:tempDesc}
                var tempArray = userProfile.projectDetails?userProfile.projectDetails:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, projectDetails:tempArray})
                setTempName("")
                setTempDesc("")
                break
            }
            case "internship": {
                let tempObj = {compName:tempIntComp, role:tempRole, duration:tempDuration, startDate:tempStartDate, endDate:tempEndDate, description:tempIntDisc}
                var tempArray = userProfile.internshipDetails?userProfile.internshipDetails:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, internshipDetails:tempArray})
                setTempComp("")
                setTempRole("")
                setTeampDUration("")
                setTempStartDate("")
                setTempEndDate("")
                setTempIntDisc("")
                break
            }
            case "certificate": {
                let tempObj = {name:certName, date:certDate, agency:certAgency}
                var tempArray = userProfile.certificationDetails?userProfile.certificationDetails:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, certificationDetails:tempArray})
                setCertName("")
                setCertDate("")
                setCertAgency("")
                break
            }
            case "papers": {
                let tempObj = {title:paperTitle, agency:paperAgency, date:paperDate, description:paperdisc}
                var tempArray = userProfile.researchPapers?userProfile.researchPapers:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, researchPapers:tempArray})
                setPaperTitle("")
                setPaperAgency("")
                setPaperDate("")
                setPaperdisc("")
                break
            }
            case "award": {
                let tempObj = {name:awardName, date:awardDate, description:awardDisc}
                var tempArray = userProfile.awards?userProfile.awards:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, awards:tempArray})
                setAwardName("")
                setAwardDate("")
                setAwardDisc("")
                break
            }
            case "additional": {
                let tempObj = {name:addName, description:addDetail}
                var tempArray = userProfile.additionalDetails?userProfile.additionalDetails:[]
                tempArray.push(tempObj)
                setUserProfile({...userProfile, additionalDetails:tempArray})
                setAddName("")
                setAddDetail("")
                break
            }
            default: {}
        }
    }

    const handleSubmit = async () => {
        if (userProfile.phoneNumber.length !== 10) {
            toast.warn("Please enter a valid Phone Number")
            return
        }
        if (userProfile.aadharNumber.length !== 12) {
            toast.warn("Please enter a valid Aadhar Card Number")
            return
        }
        var count = 0
        for (const index in Object.values(userProfile.interestedDomain)) {
            if (Object.values(userProfile.interestedDomain)[index]) {
                count++
            }
        }
        if (count === 0) {
            toast.warn("Please select at least one Interested Domain")
            return
        }
        if (userProfile.proficiency === "") {
            toast.warn("Please select your Programming Proficiency")
            return
        }
        if (userProfile.expectedPackage === "") {
            toast.warn("Please select your expected package")
            return
        }
        setUploading(true)
        setUserData(userProfile)
        await set(ref(firebaseDatabase, "STUDENTS_ARCHIVE/"+userData.department+"/"+userData.uid), userProfile)
        if (userProfile.accountType === "LINKED") {
            await set(ref(firebaseDatabase, "COLLEGE_WISE_STUDENTS/"+userData.collegeUid+"/"+userData.department+"/"+userData.collegeKey), userProfile)
        }
        toast.success("Your resume is updated.")
        setUploading(false)
    }

    const handleSelect = (item) => {
        let temp = tempArray
        // console.log(temp + " before")
        if (temp.includes(item)) {
            // console.log(temp.indexOf(item) + " index")
            temp.splice(temp.indexOf(item), 1)
            // console.log(temp + " after")
        } else {
            temp.push(item)
        }
        setTempArray(temp)
    }

    const handleDownloadResume = async () => {
        window.open(`https://resume-creator-occuhite.vercel.app/${userData.department}/${userData.uid}`)
    }

    return(
        <div className="manage-resume-container">
            
            <Header />
            
            {
                contentLoading
                ?
                <div></div>
                :
            
            <div className="download-form-container">

                <div className="download-container" onClick={handleDownloadResume} >
                    <FiDownload size={20} color="#3761EE" />
                    <p className="download-tag">Download Resume</p>
                </div>
                <Form className="resume-form-container" >

                    <p className="ete-field section-title">Personal Details</p>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">Full Name</Form.Label>
                        <Form.Control required className="field" type="text" placeholder="FirstName LastName" value={userProfile.name} onChange={(event)=>{setUserProfile({...userProfile, name:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">USN</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.usn} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Phone Number</Form.Label>
                        <Form.Control required maxLength={10} className="field" type="number-pad" placeholder="+91 xxxxx xxxxx" value={userProfile.phoneNumber} onChange={(event)=>{setUserProfile({...userProfile, phoneNumber:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">Email</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.emailId} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">About Yourself</Form.Label>
                        <Form.Control as="textarea" rows={3} maxLength={400} className="field" type="text" value={userProfile.aboutYourself} onChange={(event)=>{setUserProfile({...userProfile, aboutYourself:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">College Name</Form.Label>
                        <Form.Control required className="field" placeholder="ABC College" value={userProfile.collegeName} onChange={(event)=>{setUserProfile({...userProfile, collegeName:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Department</Form.Label>
                        <Form.Control required className="field" value={userProfile.department} disabled />             
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Semester</Form.Label>
                        <Form.Control required className="field" type="text" placeholder="7th Semester" value={userProfile.semester} onChange={(event)=>{setUserProfile({...userProfile, semester:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Gender</Form.Label>
                        <Form.Select aria-label="Default select example" value={userProfile.gender} onChange={(event)=>{setUserProfile({...userProfile, gender:event.target.value})}}>
                            <option>Select</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
                        </Form.Select>
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Graduation Year</Form.Label>
                        <Form.Control required className="field" type="text" placeholder="2022" value={userProfile.yearOfStudy} onChange={(event)=>{setUserProfile({...userProfile, yearOfStudy:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Date of Birth</Form.Label>
                        <Form.Control required className="field" type="date" placeholder="DD/MM/YYYY" value={userProfile.dateOfBirth} onChange={(event)=>{setUserProfile({...userProfile, dateOfBirth:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Aadhar Number</Form.Label>
                        <Form.Control required className="field" type="text" maxLength={12} placeholder="XXXX XXXX XXXX" value={userProfile.aadharNumber} onChange={(event)=>{setUserProfile({...userProfile, aadharNumber:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">LinkedIn Profile URL</Form.Label>
                        <Form.Control required className="field" placeholder="https://www.linkedin.com/in/....." value={userProfile.linkedInUrl} onChange={(event)=>{setUserProfile({...userProfile, linkedInUrl:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Address</Form.Label>
                        <Form.Control required className="field" placeholder="Address, Street, City, State" value={userProfile.address} onChange={(event)=>{setUserProfile({...userProfile, address:event.target.value})}} />
                    </Form.Group>

                    <p className="section-title">Education Details</p>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">10th Standard School</Form.Label>
                        <Form.Control required className="field" placeholder="ABC School" value={userProfile.tenthSchool} onChange={(event)=>{setUserProfile({...userProfile, tenthSchool:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">10th Standard Board</Form.Label>
                        <Form.Control required className="field" type="text" placeholder="CBSE/ICSE/State/Other" value={userProfile.tenthBoard} onChange={(event)=>{setUserProfile({...userProfile, tenthBoard:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">10th Standard Percentage</Form.Label>
                        <Form.Control required className="field" type="text" placeholder="XX %" value={userProfile.tenthPercentage} onChange={(event)=>{setUserProfile({...userProfile, tenthPercentage:event.target.value.replace(/[^0-9.]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">12th Standard School</Form.Label>
                        <Form.Control required className="field" placeholder="ABC School" value={userProfile.twelthSchool} onChange={(event)=>{setUserProfile({...userProfile, twelthSchool:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">12th Standard Board</Form.Label>
                        <Form.Control required className="field" type="text" placeholder="CBSE/ICSE/State/Other" value={userProfile.twelthBoard} onChange={(event)=>{setUserProfile({...userProfile, twelthBoard:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">12th Standard Percentage</Form.Label>
                        <Form.Control required className="field" type="text" placeholder="XX %" value={userProfile.twelthPercentage} onChange={(event)=>{setUserProfile({...userProfile, twelthPercentage:event.target.value.replace(/[^0-9.]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Current CGPA</Form.Label>
                        <Form.Control required className="field" type="text" placeholder="X" value={userProfile.cgpa} onChange={(event)=>{setUserProfile({...userProfile, cgpa:event.target.value.replace(/[^0-9.]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">No. of Active Backlogs</Form.Label>
                        <Form.Control required className="field" type="number-pad" placeholder="X" value={userProfile.backlogs} onChange={(event)=>{setUserProfile({...userProfile, backlogs:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <p className="section-title">Technical Skills Details</p>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Programming languages known (Optional)</Form.Label>
                        <Form.Control className="field" placeholder="C++, Java" value={userProfile.languagesKnown} onChange={(event)=>{setUserProfile({...userProfile, languagesKnown:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Technologies prevalent in</Form.Label>
                        <Form.Control required className="field" placeholder="Machine Learning, Data Science" value={userProfile.prevalentTechnologies} onChange={(event)=>{setUserProfile({...userProfile, prevalentTechnologies:event.target.value})}} />
                    </Form.Group>

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Add project details (Click "Add" to save project)</p>
                        <p className="add-button" onClick={()=>{handleAdd("project")}}>Add</p>
                    </div>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Project name</Form.Label>
                        <Form.Control className="field" placeholder="XYZ Project" value={tempName} onChange={(event)=>{setTempName(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Project description</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet." value={tempDesc} onChange={(event)=>{setTempDesc(event.target.value)}} />
                    </Form.Group>

                    <div className="ete-field" style={{marginTop:0}}>
                        <Table striped hover className="ete-field" responsive>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    !userProfile.projectDetails || userProfile.projectDetails.length === 0
                                    ?
                                    <tr>
                                        <td style={{color:"#ccc", fontWeight:600, marginTop:10}}>No projects added yet</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    :
                                    userProfile.projectDetails.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td style={{wordBreak:"break-all"}}>{item.description}</td>
                                            <td style={{cursor:"pointer", fontStyle:"italic", color:"red"}} onClick={()=>{const newFilter = userProfile.projectDetails.filter((currentItem) => currentItem !== item)
                                                setUserProfile({...userProfile, projectDetails:newFilter})}}>Remove</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    </div>

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Add internship details (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={()=>{handleAdd("internship")}}>Add</p>
                    </div>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Company Name</Form.Label>
                        <Form.Control className="field" type="text" placeholder="ABC Company" value={tempIntComp} onChange={(event)=>{setTempComp(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Role</Form.Label>
                        <Form.Control className="field" type="text" placeholder="Tester" value={tempRole} onChange={(event)=>{setTempRole(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Duration</Form.Label>
                        <Form.Control className="field" type="text" placeholder="6 months" value={tempDuration} onChange={(event)=>{setTeampDUration(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Start Date</Form.Label>
                        <Form.Control className="field" type="date" placeholder="DD/MM/YYYY" value={tempStartDate} onChange={(event)=>{setTempStartDate(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">End Date</Form.Label>
                        <Form.Control className="field" type="date" placeholder="DD/MM/YYYY" value={tempEndDate} onChange={(event)=>{setTempEndDate(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Description</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet." value={tempIntDisc} onChange={(event)=>{setTempIntDisc(event.target.value)}} />
                    </Form.Group>

                    <div className="ete-field" ><Table striped hover className="ete-field" responsive>
                        <thead>
                            <tr>
                                <th>Company Name</th>
                                <th>Role</th>
                                <th>Duration</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Description</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                !userProfile.internshipDetails || userProfile.internshipDetails.length === 0
                                ?
                                <tr>
                                    <td style={{color:"#ccc", fontWeight:600, marginTop:10}}>No internships added yet</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                :
                                userProfile.internshipDetails.map((item, index)=>{return(
                                    <tr>
                                        <td style={{wordBreak:"break-all"}}>{item.compName}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.role}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.duration}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.startDate}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.endDate}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.description}</td>
                                        <td style={{cursor:"pointer", fontStyle:"italic", color:"red",wordBreak:"break-all"}} onClick={()=>{const newFilter = userProfile.internshipDetails.filter((currentItem) => currentItem !== item)
                                            setUserProfile({...userProfile, internshipDetails:newFilter})}}>Remove</td>
                                    </tr>
                                )})
                            }
                        </tbody>
                    </Table></div>

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Add certificate details (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={()=>{handleAdd("certificate")}}>Add</p>
                    </div>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Certificate Name</Form.Label>
                        <Form.Control className="field" type="text" placeholder="Appreciation for ..." value={certName} onChange={(event)=>{setCertName(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Date of Completion</Form.Label>
                        <Form.Control className="field" type="date" placeholder="DD/MM/YYYY" value={certDate} onChange={(event)=>{setCertDate(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Issuing Agency</Form.Label>
                        <Form.Control className="field" placeholder="ABC Company" value={certAgency} onChange={(event)=>{setCertAgency(event.target.value)}} />
                    </Form.Group>

                    <div className="ete-field"><Table striped hover className="ete-field" responsive>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Certificate Name</th>
                                <th>Date</th>
                                <th>Issuing Agency</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                !userProfile.certificationDetails || userProfile.certificationDetails.length === 0
                                ?
                                <tr>
                                    <td style={{color:"#ccc", fontWeight:600, marginTop:10}}>No certificates added yet</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                :
                                userProfile.certificationDetails.map((item, index)=>{return(
                                    <tr>
                                        <td style={{wordBreak:"break-all"}}>{index+1}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.name}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.date}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.agency}</td>
                                        <td style={{cursor:"pointer", fontStyle:"italic", color:"red",wordBreak:"break-all"}} onClick={()=>{const newFilter = userProfile.certificationDetails.filter((currentItem) => currentItem !== item)
                                            setUserProfile({...userProfile, certificationDetails:newFilter})}}>Remove</td>
                                    </tr>
                                )})
                            }
                        </tbody>
                    </Table></div>

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Research paper publications (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={()=>{handleAdd("papers")}}>Add</p>
                    </div>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Title</Form.Label>
                        <Form.Control className="field" type="text" placeholder="Analysis of ..." value={paperTitle} onChange={(event)=>{setPaperTitle(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Publication</Form.Label>
                        <Form.Control className="field" type="text" placeholder="IEEE" value={paperAgency} onChange={(event)=>{setPaperAgency(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Date</Form.Label>
                        <Form.Control className="field" type="date" placeholder="DD/MM/YYYY" value={paperDate} onChange={(event)=>{setPaperDate(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Description</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet." value={paperdisc} onChange={(event)=>{setPaperdisc(event.target.value)}} />
                    </Form.Group>

                    <div className="ete-field"><Table striped hover className="ete-field" responsive>
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Agency</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                !userProfile.researchPapers || userProfile.researchPapers.length === 0
                                ?
                                <tr>
                                    <td style={{color:"#ccc", fontWeight:600, marginTop:10}}>No papers added yet</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                :
                                userProfile.researchPapers.map((item, index)=>{return(
                                    <tr>
                                        <td style={{wordBreak:"break-all"}}>{item.title}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.agency}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.date}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.description}</td>
                                        <td style={{cursor:"pointer", fontStyle:"italic", color:"red", wordBreak:"break-all"}} onClick={()=>{const newFilter = userProfile.researchPapers.filter((currentItem) => currentItem !== item)
                                            setUserProfile({...userProfile, researchPapers:newFilter})}}>Remove</td>
                                    </tr>
                                )})
                            }
                        </tbody>
                    </Table></div>

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Awards and Achievements (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={()=>{handleAdd("award")}}>Add</p>
                    </div>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Name</Form.Label>
                        <Form.Control className="field" type="text" placeholder="Award for ..." value={awardName} onChange={(event)=>{setAwardName(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Date</Form.Label>
                        <Form.Control type="date" className="field" placeholder="DD/MM/YYYY" value={awardDate} onChange={(event)=>{setAwardDate(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Description</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet" value={awardDisc} onChange={(event)=>{setAwardDisc(event.target.value)}} />
                    </Form.Group>

                    <div className="ete-field"><Table striped hover className="ete-field" responsive>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                !userProfile.awards || userProfile.awards.length === 0
                                ?
                                <tr>
                                    <td style={{color:"#ccc", fontWeight:600, marginTop:10}}>No awards added yet</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                :
                                userProfile.awards.map((item, index)=>{return(
                                    <tr>
                                        <td style={{wordBreak:"break-all"}}>{index+1}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.name}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.date}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.description}</td>
                                        <td style={{wordBreak:"break-all", cursor:"pointer", fontStyle:"italic", color:"red"}} onClick={()=>{const newFilter = userProfile.awards.filter((currentItem) => currentItem !== item)
                                            setUserProfile({...userProfile, awards:newFilter})}}>Remove</td>
                                    </tr>
                                )})
                            }
                        </tbody>
                    </Table></div>

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Additional Details (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={()=>{handleAdd("additional")}}>Add</p>
                    </div>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Name</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet." value={addName} onChange={(event)=>{setAddName(event.target.value)}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Description</Form.Label>
                        <Form.Control className="field" placeholder="Lorem ipsum dolor sit amet." value={addDetail} onChange={(event)=>{setAddDetail(event.target.value)}} />
                    </Form.Group>

                    <div className="ete-field"><Table striped hover className="ete-field">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                !userProfile.additionalDetails || userProfile.additionalDetails.length === 0
                                ?
                                <tr>
                                    <td style={{color:"#ccc", fontWeight:600, marginTop:10}}>No details added yet</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                :
                                userProfile.additionalDetails.map((item, index)=>{return(
                                    <tr>
                                        <td style={{wordBreak:"break-all"}}>{index+1}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.name}</td>
                                        <td style={{wordBreak:"break-all"}}>{item.description}</td>
                                        <td style={{wordBreak:"break-all", cursor:"pointer", fontStyle:"italic", color:"red"}} onClick={()=>{const newFilter = userProfile.additionalDetails.filter((currentItem) => currentItem !== item)
                                            setUserProfile({...userProfile, additionalDetails:newFilter})}}>Remove</td>
                                    </tr>
                                )})
                            }
                        </tbody>
                    </Table></div>

                    <p className="section-title">More about yourself</p>
                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Programming Proficiency</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group1" type='radio' checked={userProfile.proficiency==="I don't know programming"} label={"I don't know programming"} value={"I don't know programming"} onChange={(event)=>{setUserProfile({...userProfile, proficiency:event.target.value})}} />   
                            <Form.Check inline name="group1" type='radio' checked={userProfile.proficiency==="I have studied programming as a part of curriculum"} label={"I have studied programming as a part of curriculum"} value={"I have studied programming as a part of curriculum"} onChange={(event)=>{setUserProfile({...userProfile, proficiency:event.target.value})}} /> 
                            <Form.Check inline name="group1" type='radio' checked={userProfile.proficiency==="I practice coding occasionally"} label={"I practice coding occasionally"} value={"I practice coding occasionally"} onChange={(event)=>{setUserProfile({...userProfile, proficiency:event.target.value})}} /> 
                            <Form.Check inline name="group1" type='radio' checked={userProfile.proficiency==="I am a pro coder"} label={"I am a pro coder"} value={"I am a pro coder"} onChange={(event)=>{setUserProfile({...userProfile, proficiency:event.target.value})}} /> 
                        </div>
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Expected Salary Package</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group2" type='radio' checked={userProfile.expectedPackage==="2-4 L CTC"} label={"2-4 L CTC"} value={"2-4 L CTC"} onChange={(event)=>{setUserProfile({...userProfile, expectedPackage:event.target.value})}} />   
                            <Form.Check inline name="group2" type='radio' checked={userProfile.expectedPackage==="4-7 L CTC"} label={"4-7 L CTC"} value={"4-7 L CTC"} onChange={(event)=>{setUserProfile({...userProfile, expectedPackage:event.target.value})}} /> 
                            <Form.Check inline name="group2" type='radio' checked={userProfile.expectedPackage==="7-10 L CTC"} label={"7-10 L CTC"} value={"7-10 L CTC"} onChange={(event)=>{setUserProfile({...userProfile, expectedPackage:event.target.value})}} /> 
                            <Form.Check inline name="group2" type='radio' checked={userProfile.expectedPackage===">10 L CTC"} label={">10 L CTC"} value={">10 L CTC"} onChange={(event)=>{setUserProfile({...userProfile, expectedPackage:event.target.value})}} /> 
                            <Form.Check inline name="group2" type='radio' checked={userProfile.expectedPackage==="Open for all packages"} label={"Open for all packages"} value={"Open for all packages"} onChange={(event)=>{setUserProfile({...userProfile, expectedPackage:event.target.value})}} /> 
                        </div>
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">Interested Domains</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group3" type='checkbox' label={"Developer (C, C++, Java, Python)"} value={"Developer (C, C++, Java, Python"} checked={userProfile.interestedDomain.developer} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, developer:!userProfile.interestedDomain.developer}})}} />   
                            <Form.Check inline name="group3" type='checkbox' label={"Software Testing"} value={"Software Testing"} checked={userProfile.interestedDomain.testing} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, testing:!userProfile.interestedDomain.testing}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Analyst"} value={"Analyst"} checked={userProfile.interestedDomain.analyst} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, analyst:!userProfile.interestedDomain.analyst}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Data Scientist"} value={"Data Scientist"} checked={userProfile.interestedDomain.dataScientist} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, dataScientist:!userProfile.interestedDomain.dataScientist}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Dev-Ops Engineer"} value={"Dev-Ops Engineer"} checked={userProfile.interestedDomain.devOps} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, devOps:!userProfile.interestedDomain.devOps}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Full Stack Web Developer"} value={"Full Stack Web Developer"} checked={userProfile.interestedDomain.fullStackWeb} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, fullStackWeb:!userProfile.interestedDomain.fullStackWeb}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Marketing Roles"} value={"Marketing Roles"} checked={userProfile.interestedDomain.marketing} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, marketing:!userProfile.interestedDomain.marketing}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Business Development"} value={"Business Development"} checked={userProfile.interestedDomain.businessDev} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, businessDev:!userProfile.interestedDomain.businessDev}})}} /> 
                            <Form.Check inline name="group3" type='checkbox' label={"I am not sure. Open for any role."} value={"I am not sure. Open for any role."} checked={userProfile.interestedDomain.notsure} onChange={(event)=>{setUserProfile({...userProfile, interestedDomain:{...userProfile.interestedDomain, notsure:!userProfile.interestedDomain.notsure, developer:false, testing:false, analyst:false, dataScientist:false, devOps:false, fullStackWeb:false, marketing:false, businessDev:false}})}} /> 
                        </div>
                    </Form.Group>
                </Form>

                <Button variant="primary" className="ete-field submit-button" onClick={handleSubmit} >
                    {
                        uploading
                        ?
                        <Spinner animation="border" variant="light" />
                        :
                        "Save"
                    }
                </Button>
            </div>
            }

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />
        </div>
    )
}

export default ManageResume