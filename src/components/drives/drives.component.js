import React, { useEffect, useState, useContext } from "react";
import './drives.style.scss'
import { get, child, ref } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import Header from "../header/header.component";
import { BiSearchAlt } from 'react-icons/bi'
import { Table, Button } from "react-bootstrap";
import context from '../../context/app-context';
import { useNavigate } from "react-router-dom";

const Drives = ({type}) => {

    const [drivesList, setDrivesLIst] = useState([])
    const [originalList, setOriginalList] = useState([])
    const [loading, setLoading] = useState(true)
    const [searchWord, setSearchWord] = useState("")
    const [userData, setUserData] = useContext(context)
    const navigation = useNavigate()
    
    useEffect(() => {
        let temp = []

        if (type === "College") {
            get(child(ref(firebaseDatabase), "COLLEGE_VACANCY/"+userData.collegeUid)).then((snapShot) => {
                if (snapShot.exists()) {
                    for (const key in snapShot.val()) {
                        temp.push(snapShot.child(key).val())
                    }
                    setDrivesLIst(temp.reverse())
                    setOriginalList(temp)
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        } else {
            get(child(ref(firebaseDatabase), "APPROVED_VACANCY")).then((snapShot) => {
                if (snapShot.exists()) {
                    for (const key in snapShot.val()) {
                        temp.push(snapShot.child(key).val())
                    }
                    setDrivesLIst(temp.reverse())
                    setOriginalList(temp)
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        }

    }, [])

    const handleSearch = () => {
        console.log(originalList)
        let temp = []
        for (const index in originalList) {
            if (originalList[index].companyName.toLowerCase().includes(searchWord.toLowerCase()) || originalList[index].location.toLowerCase().includes(searchWord.toLowerCase()) || originalList[index].role.toLowerCase().includes(searchWord.toLowerCase())) {
                temp.push(originalList[index])
            }
        }
        setDrivesLIst(temp)
    }

    return(
        <div className="drives-component-container">
            <Header />

            <div className="search-container">
                <BiSearchAlt color="#ACACAC" size={18} />
                <input className="search-bar" placeholder="Company Name, Role or Location" type="text" value={searchWord} onKeyUp={(event)=>{if(event.key === 'Enter' || event.keyCode === 13){handleSearch()}}} onChange={(event)=>{setSearchWord(event.target.value)}} />
            </div>

            <div className="drives-list-container">
                <p className="section-title">Upcoming drives by {type}</p>

                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>Company</th>
                            <th>Role</th>
                            <th>Package</th>
                            <th>Location</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            drivesList.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.companyName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.role}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.startPackage}L - {item.endPackage}L</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.location}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" onClick={()=>{navigation("/apply", {state:{item:item, type:type}})}} variant="primary">Apply</Button></div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default Drives