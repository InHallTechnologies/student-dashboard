import React from 'react';
import './PlacementStats.style.scss';
import testimonials from '../../assets/testimonials.png';
import Marquee from "react-fast-marquee";


const PlacementStats = ({ title, list }) => {

    return (
        <div className='placement-stats-container' >
            <h2 className='grid-with-title-title'>{title}</h2>
            <Marquee gradient={false} pauseOnHover={true} pauseOnClick={true} speed={25} className='placement-stats-list-container' >
                {
                    list.map(item => <div className='testimonials-list-content' key={item.toString()} ><img className='testimonials-image' src={item} /></div>)
                }
            </Marquee>
        </div>
    )
}

export default PlacementStats;