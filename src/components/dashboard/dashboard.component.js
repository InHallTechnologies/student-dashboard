import React, { useContext, useState, useEffect} from "react";
import './dashboard.style.scss'
import { firebaseAuth, firebaseDatabase } from "../../backend/firebase-handler";
import { Image, Button, Table, Row, Modal, ModalBody } from 'react-bootstrap';
import { RiAccountCircleFill } from 'react-icons/ri'
import context from '../../context/app-context';
import { get, ref, child, query, limitToLast, remove } from 'firebase/database'
import alert from '../../assets/alert.png'
import Header from "../header/header.component";
import { useNavigate } from "react-router-dom";
import CarouselContainer from "../carousel-container/carousel-container.component";

const Dashboard = () => {

    const [userData, setUserData] = useContext(context)
    const [planDetails, setPlanDetails] = useState({})
    const [assmntResults, setAssmntResults] = useState([])
    const [allTests, setAllTests] = useState([])
    const [allPreQual, setAllPreQual] = useState([])
    const [totalApplications, setTotalApplication] = useState("-")
    const [totalPlaced, setTotalPlaced] = useState("-")
    const [collegeJobs, setCollegeJobs] = useState([])
    const [occuhireJobs, setOccuhireJobs] = useState([])
    const [confirmModal, setConfirmModal] = useState(false)
    const [selectedTest, setSelectedTest] = useState({})
    const [type, setType] = useState("")
    const navigation = useNavigate()

    useEffect(() => {
        // firebaseAuth.signOut()
        let applications = 0
        let placed = 0
        let tempResults = []
        let allTests = []
        let allPre = []
        const tempCollegeJobs = []
        const tempOccuhireJobs = []

        get(child(ref(firebaseDatabase), "STUDENT_PLAN_DETAILS/"+userData.plan)).then((snapshot) => {
            if (snapshot.exists()) {
                setPlanDetails(snapshot.val())
            }
        })    
        
        get(child(ref(firebaseDatabase), "APPLICATION_STATUS/"+userData.uid)).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    applications++
                    if (snapShot.child(key).child("status").val() === "Placed") {
                        placed++
                    }
                }
                setTotalApplication(applications.toString())
                setTotalPlaced(placed.toString())
            }
        })

        get(child(ref(firebaseDatabase), "ASSESSMENT_RESULTS/"+userData.uid)).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    tempResults.push(snapShot.child(key).val())
                }
                setAssmntResults(tempResults.reverse())
            }
        })
        const querySt = query(ref(firebaseDatabase, "ASSESSMENT_DETAILS"), limitToLast(5))
        get(querySt).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    // if (allTests.length<5) {
                    //     allTests.push(snapShot.child(key).val())
                    // }
                    allTests.push(snapShot.child(key).val())
                }
                setAllTests(allTests.reverse())
            }
        })
        const queryPreQual = query(ref(firebaseDatabase, "PREQUALIFYING_EXAMS"), limitToLast(5))
        get(queryPreQual).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    allPre.push(snapShot.child(key).val())
                }
                setAllPreQual(allPre.reverse())
            }
        })
        if (userData.accountType === "LINKED") {
            get(child(ref(firebaseDatabase), "COLLEGE_VACANCY/"+userData.collegeUid)).then((snapSHot) => {
                if (snapSHot.exists()) {
                    for (const key in snapSHot.val()) {
                        tempCollegeJobs.push(snapSHot.child(key).val())
                    }
                }
                setCollegeJobs(tempCollegeJobs.reverse())
            })
        }
        get(child(ref(firebaseDatabase), "APPROVED_VACANCY")).then((snapSHot) => {
            if (snapSHot.exists()) {
                for (const key in snapSHot.val()) {
                    tempOccuhireJobs.push(snapSHot.child(key).val())
                }
            }
            setOccuhireJobs(tempOccuhireJobs.reverse())
        })

    },[])

    const handleTest = async () => {
        for (const index in assmntResults) {
            if (assmntResults[index].id === selectedTest.id) {
                const date1 = new Date(assmntResults[index].date.split(" ")[0])
                var date = new Date().getDate(); 
                var month = new Date().getMonth() + 1; 
                var year = new Date().getFullYear(); 
                const date2 = new Date(month + "/" + date + "/" + year)
                const diffTime = Math.abs(date2 - date1);
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                console.log(diffDays)
                if (diffDays <= 15) {
                    alert("Sorry, you took this assessment less than 15 days ago. You will have to wait for " + (15-diffDays) + " days before you can take this test again.")
                    return
                }
                await remove(ref(firebaseDatabase, "ASSESSMENT_RESULTS/"+userData.uid+"/"+selectedTest.id))
                navigation('/test',{state:{item:selectedTest, type:type}})
                return
            }
        }
        await remove(ref(firebaseDatabase, "ASSESSMENT_RESULTS/"+userData.uid+"/"+selectedTest.id))
        navigation('/test',{state:{item:selectedTest, type:type}})
    }

    return(
        <div className='dashboard-container'>
                
            <Header />

            <CarouselContainer/>

            <div className="stats-container">
                <div className="tag-data-container">
                    <p className="tag">OccuHire opportunities available</p>
                    <p className="data">{planDetails.numberOfOptions}</p>
                    <p className="conclusion" style={{cursor:"pointer"}} onClick={_ => navigation('/upgrade-plan')}>{planDetails.upgradable?"Get more":""}</p>
                </div>
                {/* <div className="stat-bar"></div> */}
                <div className="tag-data-container">
                    <p className="tag">OccuHire opportunities used</p>
                    <p className="data">{totalApplications}</p>
                    <p className="conclusion" style={{display:planDetails.upgradable?null:"none"}}>{totalApplications==="-"?"-":((planDetails.numberOfOptions-totalApplications)/planDetails.numberOfOptions)*100}% left</p>
                </div>
                {/* <div className="stat-bar"></div> */}
                <div className="tag-data-container">
                    <p className="tag">Companies placed in</p>
                    <p className="data">{totalPlaced}</p>
                    <p className="conclusion">{totalPlaced==="-"?"-":Math.round((totalPlaced/totalApplications)*100)}% placement rate</p>
                </div>
            </div>

                <div className="skill-test-container">
                    
                    <div className="take-assessment-container-dash">
                        <p className="section-title">Pre-qualifying exams</p>
                        <div className="section-bar"></div>

                        <div>
                            {
                                allPreQual.map((item, index) => {return(
                                    <div onClick={()=>{setSelectedTest(item); setType("PRE"); setConfirmModal(true)}} style={{display:"flex", flexDirection:"row", paddingLeft:15, paddingRight:15, marginTop:25, cursor:"pointer"}}>
                                        <img src={item.courseLogo} alt="course-icon" style={{width:25, height:25, marginRight:10}} />
                                        <p style={{fontSize:18, color:"#444", fontWeight:500}}>{item.courseName}</p>
                                    </div>
                                )})
                            }
                        </div>
                    </div>
                    <div className="take-assessment-container-dash">
                        <p className="section-title">Skill assessment</p>
                        <div className="section-bar"></div>

                        <div>
                            {
                                allTests.map((item, index) => {return(
                                    <div onClick={()=>{setSelectedTest(item); setType("SKILL"); setConfirmModal(true)}} style={{display:"flex", flexDirection:"row", paddingLeft:15, paddingRight:15, marginTop:25, cursor:"pointer"}}>
                                        <img src={item.courseLogo} alt="course-icon" style={{width:25, height:25, marginRight:10}} />
                                        <p style={{fontSize:18, color:"#444", fontWeight:500}}>{item.courseName}</p>
                                    </div>
                                )})
                            }
                        </div>
                    </div>
                </div>

                <div className="skill-badge-container" style={{marginRight:10}}>
                        <p className="section-title">Skill badges</p>
                        <div className="section-bar"></div>

                        <div className="result-container">
                            {
                                assmntResults.length === 0
                                ?
                                <p className="no-badges">No badges yet. Take an assessment now!</p>
                                :
                                assmntResults.map((item, index) => {return(
                                    <div className="result-arch">
                                        <div style={item.badge==="green"?{backgroundColor:"#82ECAC"}:item.badge==="orange"?{backgroundColor:"#FEE4A3"}:{backgroundColor:"#FF8585"}} className="result-back"><p className="result-badge" style={item.badge==="green"?{color:"#308B55"}:item.badge==="orange"?{color:"#E4A400"}:{color:"#D43B3B"}}>{item.percentage} %</p></div>
                                        <p className="result-course">{item.courseName}</p>
                                    </div>
                                )})
                            }
                        </div>
                    </div>

                {
                    (userData.accountType === "VIA-COLLEGE" || userData.accountType === "LINKED")
                    &&
                    <div className="drives-container">
                        <p className="section-title">Upcoming drives by College</p>
                        
                        <Table hover className="table" responsive>
                            <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Role</th>
                                    <th>Package</th>
                                    <th>Location</th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody className="table-body">
                                {
                                    collegeJobs.map((item, index) => {return(
                                        <tr>
                                            <td>
                                                <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                                    <img src={item.companyLogo} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                                    <p>{item.companyName}</p>
                                                </div>
                                            </td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.role}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.startPackage}L - {item.endPackage}L</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.location}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" variant="primary" onClick={()=>{navigation("/apply", {state:{item:item, type:"College"}})}}>Apply</Button></div></td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    </div>
                }
                
            <div className="drives-container">
                <p className="section-title">Upcoming drives by OccuHire</p>

                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>Company</th>
                            <th>Role</th>
                            <th>Package</th>
                            <th>Location</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            occuhireJobs.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.companyName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.role}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.startPackage}L - {item.endPackage}L</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.location}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" variant="primary" onClick={()=>{navigation("/apply", {state:{item:item, type:"OccuHire"}})}}>Apply</Button></div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            </div>

            <Modal show={confirmModal} onHide={()=>{setConfirmModal(false)}}>
                <Modal.Header closeButton>
                <Modal.Title>Take test?</Modal.Title>
                </Modal.Header>
                <Modal.Body>Please read the below instructions carefully.</Modal.Body>
                <ModalBody>
                    <div>
                        <div style={{display:"flex", flexDirection:"row", marginBottom:10}}>
                            <img src={alert} style={{width:12, height:12, marginRight:5}} />
                            <p style={{margin:0, padding:0, marginTop:'-5px'}}>Clicking on "Continue" will begin the test.</p>
                        </div>
                        <div style={{display:"flex", flexDirection:"row", marginBottom:10}}>
                            <img src={alert} style={{width:12, height:12, marginRight:5}} />
                            <p style={{margin:0, padding:0, marginTop:'-5px'}}>Hitting back at any moment before submitting will auto-submit the test.</p>
                        </div>
                        <div style={{display:"flex", flexDirection:"row", marginBottom:10}}>
                            <img src={alert} style={{width:12, height:12, marginRight:5}} />
                            <p style={{margin:0, padding:0, marginTop:'-5px'}}>Refreshing the page at any moment before submitting will auto-submit the test.</p>
                        </div>
                    </div>
                </ModalBody>
                <Modal.Footer>
                <Button variant="secondary" onClick={()=>{setConfirmModal(false)}}>
                    Cancel
                </Button>
                <Button variant="primary" onClick={()=>{handleTest(); setConfirmModal(false)}}>
                    Continue
                </Button>
                </Modal.Footer>
            </Modal>

        </div>
    )
}

export default Dashboard