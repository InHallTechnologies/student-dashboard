import React, { useContext, useEffect, useState } from "react";
import { firebaseAuth, firebaseDatabase } from "../../backend/firebase-handler";
import './home.style.scss'
import { Image } from 'react-bootstrap';
import logo from '../../assets/logo.png';
import { Helmet } from 'react-helmet';
import SidebarNavigation from '../../components/sidebar-navigation/sidebar-navigation.component';
import { useNavigate, useLocation } from 'react-router-dom';
import Dashboard from "../../components/dashboard/dashboard.component";
import ManageResume from "../../components/manage-resume/manage-resume.component";
import Drives from "../../components/drives/drives.component";
import PlacementStatus from "../../components/placement-status/placement-status.component";
import TakeAssessment from "../../components/take-assessment/take-assessment.component";
import LinkCollege from "../../components/link-college/link-college.component";
import CollapsableNavigation from "../../components/collapsable-navigation/collapsable-navigation.component";
import AssessmentResult from "../../components/assessment-result/assessment-result.component";
import { child, get, ref } from "firebase/database";
import context from "../../context/app-context";

const HomePage = ({setCurrentState}) => {

    const firebaseUser = firebaseAuth.currentUser;
    const navigate = useNavigate();
    const location = useLocation()
    const [selectedTab, setSelectedTab] = useState("Dashboard")
    const [userData, setUserData] = useContext(context)

    useEffect(() => {
        
        get(child(ref(firebaseDatabase), "STUDENTS_ARCHIVE/"+firebaseUser.displayName+"/"+firebaseUser.uid)).then((snap)=>{
            if (snap.exists()) {
                setUserData(snap.val())
            } else {
                window.location.reload(false)
            }
        })

        if (!firebaseUser){
            setCurrentState("LOGIN")
        }

        if (location.search){
            const params = new URLSearchParams(location.search);
            setSelectedTab(params.get('tab'));
        }
    },[])

    const handleChange = (option) => {
        setSelectedTab(option)
    }
 
    return(
        <div className='home-container'>
            <Helmet>
                {
                    firebaseUser
                    ?
                    <title>OccuHire </title>
                    :
                    <title>OccuHire</title>
                }
                
            </Helmet>
            
            <div className='sidebar'>
                <Image className='logo' src={logo} alt='OccuHire' />
                <SidebarNavigation onChange={handleChange} />
            </div>

            <div className="collapsible-menu-container">
                <CollapsableNavigation onChange={handleChange} />
            </div>


            
            <div className='content'>
                {
                    selectedTab === "Dashboard"
                    &&
                    <Dashboard />
                }
                {
                    selectedTab === "DrivesByCollege"
                    &&
                    <Drives type="College" />
                }
                {
                    selectedTab === "DrivesByOccuHire"
                    &&
                    <Drives type="OccuHire" />
                }
                {
                    selectedTab === "PlacementStatus"
                    &&
                    <PlacementStatus />
                }   
                {
                    selectedTab === "AssessmentResults"
                    &&
                    <AssessmentResult />
                }
                {
                    selectedTab === "TakeAssessment"
                    &&
                    <TakeAssessment />
                }
                {
                    selectedTab === "LinkMyCollege"
                    &&
                    <LinkCollege />
                }
                {
                    selectedTab === "ManageResume"
                    &&
                    <ManageResume />
                }
            </div>

        </div>
    )
}

export default HomePage