import React, { createRef, useContext, useEffect, useRef, useState } from "react";
import { Form, Button, Spinner } from "react-bootstrap";
import './test.style.scss'
import { get, ref, child, set, push } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import { useParams, useNavigate, useLocation } from "react-router";
import logo from '../../assets/logo.png'
import Header from "../../components/header/header.component";
import context from "../../context/app-context"
import { Offline, Online } from "react-detect-offline";
import offline from '../../assets/offline.png'

const TestPage = (props) => {

    const location = useLocation();
    const type = location.state.type
    const [courseDetail, setCOurseDetail] = useState(location.state.item)
    const [laoding, setLoading] = useState(true)
    const [submitted, setSubmitted] = useState(false)
    const [score, setScore] = useState("")
    const [userData, setUserData] = useContext(context)
    const [uploading, setUploading] = useState(false)
    const studentDetail = {name:userData.name, semester:userData.semester, collegeName:userData.collegeName, department:userData.department, usn:userData.usn, yearOfStudy:userData.yearOfStudy, phoneNumber:userData.phoneNumber, emailId:userData.emailId, collegeUid:userData.collegeUid}
    courseDetail.student = studentDetail
    const Ref = createRef(null);
    const [timer, setTimer] = useState('00:00:00');
    const navigation = useNavigate()

    useEffect(()=>{  
        setCOurseDetail(location.state.item)

        get(child(ref(firebaseDatabase), "ASSESSMENT_RESULTS/"+userData.uid+"/"+courseDetail.id)).then((snapShot)=>{
            if(snapShot.exists()) {
                navigation(-1)
            } else {
                clearTimer(getDeadTime());
                var count=0
                for (const index in courseDetail.questions) {
                    if (courseDetail.questions[index].selected === courseDetail.questions[index].rightAnswere) {
                        count++
                    }
                }
                var date = new Date().getDate(); 
                var month = new Date().getMonth() + 1; 
                var year = new Date().getFullYear(); 
                var hours = new Date().getHours(); 
                var min = new Date().getMinutes();
                var sec = new Date().getSeconds(); 
                courseDetail.date = month + "/" + date + "/" + year + " " + hours + ":" + min + ":" + sec
                courseDetail.scoreAchieved = count.toString()
                courseDetail.percentage = Math.round(((parseInt(count)/parseInt(courseDetail.totalScore))*100))
                courseDetail.student = studentDetail
                if(parseInt(courseDetail.percentage) >= 90) {
                    courseDetail.badge = "green"
                } else if (parseInt(courseDetail.percentage) < 90 && parseInt(courseDetail.percentage) >= 50) {
                    courseDetail.badge = "orange"
                } else {
                    courseDetail.badge = "red"
                }
                courseDetail.type = type
                courseDetail.timeStamp = push(ref(firebaseDatabase, "ASSESSMENT_RESULTS/"+userData.uid)).key
                set(ref(firebaseDatabase, "ASSESSMENT_RESULTS/"+userData.uid+"/"+courseDetail.id), courseDetail)
                setLoading(false)
            }
        })

        return (()=>{
            clearInterval(Ref.current)
        })
    }, [])
  
    const getTimeRemaining = (e) => {
        const total = Date.parse(e) - Date.parse(new Date());
        const seconds = Math.floor((total / 1000) % 60);
        const minutes = Math.floor((total / 1000 / 60) % 60);
        const hours = Math.floor((total / 1000 * 60 * 60) % 24);
        return {
            total, hours, minutes, seconds
        };
    }

    const startTimer = (e) => {
        let { total, hours, minutes, seconds } 
                    = getTimeRemaining(e);
        if (total >= 0) {
            setTimer(
                (hours > 9 ? hours : '0' + hours) + ':' +
                (minutes > 9 ? minutes : '0' + minutes) + ':'
                + (seconds > 9 ? seconds : '0' + seconds)
            )
        } else {
            calc()
        }
    }

    const getDeadTime = () => {
        let deadline = new Date();
        deadline.setSeconds(deadline.getSeconds() + courseDetail.duration*60);
        return deadline;
    }

    const clearTimer = (e) => {
        if (Ref.current) clearInterval(Ref.current);
        const id = setInterval(() => {
            startTimer(e);
        }, 1000)
        Ref.current = id;
    }

    const handleSelect = async (item, index, value) => {
        courseDetail.questions[index].selected = value
        var count=0
        for (const index in courseDetail.questions) {
            if (courseDetail.questions[index].selected === courseDetail.questions[index].rightAnswere) {
                count++
            }
        }
        var date = new Date().getDate(); 
        var month = new Date().getMonth() + 1; 
        var year = new Date().getFullYear(); 
        var hours = new Date().getHours(); 
        var min = new Date().getMinutes();
        var sec = new Date().getSeconds(); 
        courseDetail.date = month + "/" + date + "/" + year + " " + hours + ":" + min + ":" + sec
        courseDetail.scoreAchieved = count.toString()
        courseDetail.percentage = Math.round(((parseInt(count)/parseInt(courseDetail.totalScore))*100))
        courseDetail.student = studentDetail
        if(parseInt(courseDetail.percentage) >= 90) {
            courseDetail.badge = "green"
        } else if (parseInt(courseDetail.percentage) < 90 && parseInt(courseDetail.percentage) >= 50) {
            courseDetail.badge = "orange"
        } else {
            courseDetail.badge = "red"
        }
        courseDetail.type = type
        courseDetail.timeStamp = push(ref(firebaseDatabase, "ASSESSMENT_RESULTS/"+userData.uid)).key
        await set(ref(firebaseDatabase, "ASSESSMENT_RESULTS/"+userData.uid+"/"+courseDetail.id), courseDetail)
    }     

    const calc = async () => {
        setUploading(true)        
        var count=0
        for (const index in courseDetail.questions) {
            if (courseDetail.questions[index].selected === courseDetail.questions[index].rightAnswere) {
                count++
            }
        }
        var date = new Date().getDate(); 
        var month = new Date().getMonth() + 1; 
        var year = new Date().getFullYear(); 
        var hours = new Date().getHours(); 
        var min = new Date().getMinutes();
        var sec = new Date().getSeconds(); 
        courseDetail.date = month + "/" + date + "/" + year + " " + hours + ":" + min + ":" + sec
        courseDetail.scoreAchieved = count.toString()
        courseDetail.percentage = Math.round(((parseInt(count)/parseInt(courseDetail.totalScore))*100))
        courseDetail.student = studentDetail
        if(parseInt(courseDetail.percentage) >= 90) {
            courseDetail.badge = "green"
        } else if (parseInt(courseDetail.percentage) < 90 && parseInt(courseDetail.percentage) >= 50) {
            courseDetail.badge = "orange"
        } else {
            courseDetail.badge = "red"
        }
        courseDetail.type = type
        courseDetail.timeStamp = push(ref(firebaseDatabase, "ASSESSMENT_RESULTS/"+userData.uid)).key
        await set(ref(firebaseDatabase, "ASSESSMENT_RESULTS/"+userData.uid+"/"+courseDetail.id), courseDetail)
        setScore(count.toString())
        clearInterval(Ref.current)
        setSubmitted(true)
    }

    return(
        <div className="test-container">
            <div className="side-bar-container">
                <img className="logo" src={logo} alt="OccuHire" />
                <p className="detail-tag">Course name</p>
                <p className="detail-value">{courseDetail.courseName}</p>
                <p className="detail-tag">Total questions</p>
                <p className="detail-value">{courseDetail.questions.length}</p>
                <p className="detail-tag">Total score</p>
                <p className="detail-value">{courseDetail.totalScore}</p>
                <p className="detail-tag">Duration</p>
                <p className="detail-value">{courseDetail.duration} minutes</p>
                <p className="detail-tag" style={{marginTop:30, color:"#d92727",display:submitted?"none":null}}>Time Remaining</p>
                <p className="timer" style={{display:submitted?"none":null}} >{timer}</p>
                {
                    submitted
                    &&
                    <div className="result-container">
                        <p className="result-tag">Score Achieved</p>
                        <p className="result-value">{score}</p>
                    </div>
                }
                
            </div>
            <div className="header-test-container">
                <div className="responsive-header-container">
                    <Header />
                </div>

                <Online>
                    <div className="questions-container">
                        <p className="section-title">Answer all the questions and submit the test</p>
                        {
                            !laoding
                            &&
                            <div>
                                <Form className="form-container">
                                    {
                                        courseDetail.questions.map((item, index)=>{return(
                                            <Form.Group className="question-answer-container" controlId="exampleForm.ControlInput1">
                                                <pre className="question">{`${index+1}. ${item.question}`}</pre>
                                                <div className="option-container">
                                                    <Form.Check disabled={submitted || uploading} name={index} className="options" onChange={(event)=>{handleSelect(item, index, event.target.value)}} type='radio' label={item.option1} value={item.option1} />   
                                                    <Form.Check disabled={submitted || uploading} name={index} className="options" onChange={(event)=>{handleSelect(item, index, event.target.value)}} type='radio' label={item.option2} value={item.option2} /> 
                                                    <Form.Check disabled={submitted || uploading} name={index} className="options" onChange={(event)=>{handleSelect(item, index, event.target.value)}} type='radio' label={item.option3} value={item.option3} />
                                                    <Form.Check disabled={submitted || uploading} name={index} className="options" onChange={(event)=>{handleSelect(item, index, event.target.value)}} type='radio' label={item.option4} value={item.option4} /> 
                                                </div>
                                                {
                                                    submitted
                                                    &&
                                                    <Form.Text style={item.selected===item.rightAnswere?{color:"#308B55", backgroundColor:"#82FFB4"}:{color:"#D43B3B", backgroundColor:"#FFCFCF"}} className="result">Correct answer - {item.rightAnswere}</Form.Text>
                                                }
                                            </Form.Group>
                                        )})
                                    }
                                </Form>

                                <Button onClick={calc} disabled={submitted || uploading} className="submit-button" variant="primary">
                                    {
                                        uploading
                                        ?
                                        <Spinner variant="light"/>
                                        :
                                        "Submit"
                                    }
                                </Button>
                            </div>                            
                        }
                    </div>
                </Online>
                
                <Offline>
                    <div className="no-internet-container">
                        <img alt="offline" className="offline-image" src={offline} />
                        <p className="offline-tag">Offline</p>
                        <p className="offline-subtag">Slow or no internet connection.</p>
                        <p className="offline-subtag">Please check your internet settings</p>
                    </div>
                </Offline>
            </div>
            
        </div>
    )
}

export default TestPage