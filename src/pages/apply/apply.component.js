import React, { useContext, useEffect, useState } from "react";
import './apply.style.scss'
import logo from '../../assets/logo.png'
import { useLocation, useNavigate } from "react-router-dom";
import { Button, Form, Modal, Spinner } from "react-bootstrap";
import 'react-toastify/dist/ReactToastify.css';
import { toast, ToastContainer } from "react-toastify";
import context from "../../context/app-context";
import { set, ref, get, child } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import { IoMdArrowRoundBack } from 'react-icons/io'

const ApplyPage = () => {

    const location = useLocation()
    const navigate = useNavigate()
    const [jobDetail, setJobDetail] = useState(location.state.item)
    const [buttonTag, setButtonTag] = useState("Check Eligibility")
    const [userData, setUserData] = useContext(context)
    const [loading, setLoading] = useState(false)
    const [allowed, setAllowed] = useState("")
    const [years, setYears] = useState("")
    const [preQuals, setPreQuals] = useState("")
    const [skills, setSkills] = useState("")
    const [numberOfAppl, setNoOfAppl] = useState(0)
    const [available, setAvailabe] = useState(50)
    const type = location.state.type
    const [resultList, setResultList] = useState([])
    const [modalVisibility, setModalVisibility] = useState(false)

    useEffect(()=>{
        let branchesTemp = ""
        let yearsTemp = ""
        let preTemp = ""
        let skillTemp = ""
        let tempResult = []

        get(child(ref(firebaseDatabase), "ASSESSMENT_RESULTS/"+userData.uid)).then((snap)=>{
            if (snap.exists()) {
                for (const key in snap.val()) {
                    tempResult.push(snap.child(key).val())
                }
                setResultList(tempResult)
            }
        })

        get(child(ref(firebaseDatabase), "APPLICATION_STATUS/"+userData.uid)).then((snap)=>{
            if (snap.exists()) {
                let count = 0
                for (const key in snap.val()) {
                    if (snap.child(key).child("type").val() === "OccuHire") {
                        count++
                    }
                }
                setNoOfAppl(count)
            }
        })

        get(child(ref(firebaseDatabase), "STUDENT_PLAN_DETAILS/"+userData.plan)).then((snap)=>{
            if (snap.exists()) {
                setAvailabe(parseInt(snap.child("numberOfOptions").val()))
            }
        })

        for (const index in jobDetail.allowedBranches) {
            branchesTemp += jobDetail.allowedBranches[index].name + ", "
        }
        setAllowed(branchesTemp.substring(0, branchesTemp.length-2))
        for (const index in jobDetail.graduationYear) {
            yearsTemp += jobDetail.graduationYear[index].name + ", "
        }
        setYears(yearsTemp.substring(0, yearsTemp.length-2))
        for (const index in jobDetail.preQualifyingExams) {
            preTemp += jobDetail.preQualifyingExams[index].name + ", "
        }
        setPreQuals(preTemp.substring(0, preTemp.length-2))
        for (const index in jobDetail.skillAssessments) {
            skillTemp += jobDetail.skillAssessments[index].name + ", "
        }
        setSkills(skillTemp.substring(0, skillTemp.length-2))
    }, [])

    const handleCLick = async (event) => {
        event.preventDefault()

        if (buttonTag === "Check Eligibility") {
            await get(child(ref(firebaseDatabase), "APPLICATION_STATUS/"+userData.uid+"/"+jobDetail.jobId)).then((snap)=>{
                if (snap.exists() && (snap.child("status").val()==="Applied" || snap.child("status").val()==="Processing")) {
                    toast.error("Sorry, you already have a pending application for this job posting.")
                    return
                } else {
                    if (type==="OccuHire" && userData.plan==="Free Plan" && parseInt(parseFloat(jobDetail.endPackage)*100000)>400000) {
                        toast.error("Your current plan does not allow you to apply for jobs with package >4L. Upgrade your plan now.")
                        return
                    }
                    if (type==="OccuHire" && userData.plan==="Basic Plan" && parseInt(parseFloat(jobDetail.endPackage)*100000)>750000) {
                        toast.error("Your current plan does not allow you to apply for jobs with package >7.5L. Upgrade your plan now.")
                        return
                    }
                    if (userData.cgpa === "" || (parseFloat(userData.cgpa) < parseFloat(jobDetail.cutoffCGPA))) {
                        toast.error("Your current CGPA is less than cut-off CGPA")
                        return
                    }
                    if (userData.tenthPercentage === "" || (parseFloat(userData.tenthPercentage) < parseFloat(jobDetail.min10))) {
                        toast.error("Your 10th Percentage is less than cut-off")
                        return
                    }
                    if (userData.twelthPercentage === "" || (parseFloat(userData.twelthPercentage) < parseFloat(jobDetail.min12))) {
                        toast.error("Your 12th Percentage is less than cut-off")
                        return
                    }
                    if (!jobDetail.allBranches && !allowed.includes(userData.department)) {
                        toast.error("Your branch is not present in 'Allowed Branches'")
                        return
                    }
                    if (!years.includes(userData.yearOfStudy)) {
                        toast.error("Your year of graduation does not qualify for the job")
                        return
                    }
                    if (jobDetail.preQualifyingExams && jobDetail.preQualifyingExams !== undefined) {
                        let resultPreQuals = []
                        for (const index in jobDetail.preQualifyingExams) {
                            for (const resultIndex in resultList) {
                                if (resultList[resultIndex].id === jobDetail.preQualifyingExams[index].id) {
                                    let tempSKillObject = {name:resultList[resultIndex].courseName, id:resultList[resultIndex].id, score:resultList[resultIndex].percentage}
                                    resultPreQuals.push(tempSKillObject)
                                }
                            }
                        }
                        if (jobDetail.preQualifyingExams.length !== resultPreQuals.length) {
                            toast.error("You haven't taken all the required pre-qualifying exams")
                            return
                        }
                        jobDetail.preQualResult = resultPreQuals
                        userData.preQualResult = resultPreQuals
                    }

                    if (jobDetail.skillAssessments && jobDetail.skillAssessments !== undefined) {
                        let resultSkills = []
                        for (const index in jobDetail.skillAssessments) {
                            for (const resultIndex in resultList) {
                                if (resultList[resultIndex].id === jobDetail.skillAssessments[index].id) {
                                    let tempSKillObject = {name:resultList[resultIndex].courseName, id:resultList[resultIndex].id, score:resultList[resultIndex].percentage}
                                    resultSkills.push(tempSKillObject)
                                }
                            }
                        }
                        
                        if (jobDetail.skillAssessments.length !== resultSkills.length) {
                            toast.error("You haven't taken all the required skill assessments")
                            return
                        }
                        jobDetail.skillResult = resultSkills
                        userData.skillResult = resultSkills
                    }

                    var date = new Date().getDate(); 
                    var month = new Date().getMonth() + 1; 
                    var year = new Date().getFullYear(); 
                    var hours = new Date().getHours(); 
                    var min = new Date().getMinutes();
                    var sec = new Date().getSeconds(); 
                    jobDetail.applicationDate = date + "-" + month + "-" + year + " " + hours + ":" + min + ":" + sec
                    var testDate1Array = jobDetail.lastDateToApply.split("-")
                    var testDate1 = testDate1Array[1]+"-"+testDate1Array[2]+"-"+testDate1Array[0]
                    var testDate2 = month+"-"+date+"-"+year
                    const diffTime = (new Date(testDate1) - new Date(testDate2));
                    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                    if (diffDays < 0) {
                        toast.error("Sorry, the last date to apply for this vacancy has passed.")
                        return
                    }
                    toast.success("Your are eligible for this job.")
                    setButtonTag("Apply for job")
                }
            })
            
        } else {
            setLoading(true)
            if (userData.plan === "Basic Plan" && numberOfAppl === available) {
                toast.error("Sorry, your plan only allows " + available + " job applications. Upgrade your account to apply for this vacancy.")
                setLoading(false)
                return
            } 
            jobDetail.studentResume = userData
            jobDetail.status = "Applied"
            userData.status = "Applied"
            jobDetail.type = type
            await set(ref(firebaseDatabase, "APPLICATION_STATUS/"+userData.uid+"/"+jobDetail.jobId), jobDetail)
            if (type === "College") {
                await set(ref(firebaseDatabase, "COLLEGE_APPLICANTS_LIST/"+userData.collegeUid+"/"+jobDetail.companyUID+"/"+jobDetail.jobId+"/"+userData.uid), userData)
            } else {
                await set(ref(firebaseDatabase, "COMPANY_APPLICANTS_LIST/"+jobDetail.companyUID+"/"+jobDetail.jobId+"/"+userData.uid), userData)
            }
            console.log(jobDetail.registrationLink)
            if (jobDetail.registrationLink === "") {
                toast.success("You have successfully applied for the job. Please wait while your application is reviewed.")
            } else {
                setModalVisibility(true)
            }
        }
    }

    return(
        <div className="apply-container">
            
            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>

            <div className="form-button-container">
                <Form className="job-detail-container">
                    <p className="section-title ete-field" style={{marginLeft:'0'}}>{jobDetail.companyName} - Job description and cutoffs</p>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Role</Form.Label>
                        <Form.Control className="field" value={jobDetail.role} disabled />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Location</Form.Label>
                        <Form.Control className="field" value={jobDetail.location} disabled />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Description</Form.Label>
                        <Form.Control className="field" value={jobDetail.jobDescription} disabled />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Start Package (INR)</Form.Label>
                        <Form.Control className="field" value={jobDetail.startPackage + " LPA"} disabled />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">End Package (INR)</Form.Label>
                        <Form.Control className="field" value={jobDetail.endPackage + " LPA"} disabled />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Total Openings</Form.Label>
                        <Form.Control className="field" value={jobDetail.totalOpenings} disabled />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Cut-off CGPA</Form.Label>
                        <Form.Control className="field" value={jobDetail.cutoffCGPA} disabled />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Minimum 10th Percentage</Form.Label>
                        <Form.Control className="field" value={jobDetail.min10} disabled />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Minimum 12th Percentage</Form.Label>
                        <Form.Control className="field" value={jobDetail.min12} disabled />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Allowed Branches</Form.Label>
                        <Form.Control className="field" value={jobDetail.allBranches?"All Branches":allowed} disabled />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Preferred Skills</Form.Label>
                        <Form.Control className="field" value={jobDetail.preferredSkills} disabled />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Graduation year</Form.Label>
                        <Form.Control className="field" value={years} disabled />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Service Years</Form.Label>
                        <Form.Control className="field" value={jobDetail.numberOfYears===""?"No agreement":jobDetail.numberOfYears} disabled />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Last Date to Apply</Form.Label>
                        <Form.Control className="field" value={jobDetail.lastDateToApply.split("-").reverse().join("-")} disabled />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Pre-qualifying exams</Form.Label>
                        <Form.Control className="field" value={preQuals} disabled />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Skill assessments</Form.Label>
                        <Form.Control className="field" value={skills} disabled />
                    </Form.Group>                
                </Form>

                <Button type="submit" variant="primary" className="button" onClick={handleCLick} >
                    {
                        loading
                        ?
                        <Spinner variant="primary" />
                        :
                        buttonTag
                    }
                </Button>
            </div>

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />

            <Modal show={modalVisibility} onHide={()=>{setModalVisibility(false)}}>
                <Modal.Header closeButton >
                  <Modal.Title>Applied successfully</Modal.Title>
                </Modal.Header>
                                
                <Modal.Body>
                  <p>You have successfully applied for the job. Please visit the link {jobDetail.registrationLink} and register yourself.</p>
                </Modal.Body>
                                
                <Modal.Footer>
                  <Button variant="secondary" onClick={()=>{setModalVisibility(false)}}>Close</Button>
                  <Button variant="primary" onClick={()=>{window.open(jobDetail.registrationLink, "_blank")}}>Register now!</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default ApplyPage