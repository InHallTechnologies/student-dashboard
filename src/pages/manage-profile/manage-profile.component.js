import React, { useContext, useState } from "react";
import './manage-profile.style.scss'
import logo from '../../assets/logo.png'
import { Button, Form, Modal, Spinner, Image } from "react-bootstrap";
import 'react-toastify/dist/ReactToastify.css';
import { toast, ToastContainer } from "react-toastify";
import context from "../../context/app-context";
import { sendPasswordResetEmail } from "firebase/auth";
import { FiLogOut } from 'react-icons/fi'
import { ref as firebaseStorageRef, uploadBytesResumable, getDownloadURL } from 'firebase/storage';
import { get, ref, child, set } from 'firebase/database'
import { firebaseAuth, firebaseDatabase, firebaseStorage } from "../../backend/firebase-handler";
import { IoMdArrowRoundBack } from 'react-icons/io'
import { useNavigate } from "react-router-dom";

const ManageProfile = () => {
    
    const [userData, setUserData] = useContext(context)
    const [userProfile, setUserProfile] = useState(userData)
    const [loading, setLoading] = useState(false)
    const [modalVisibility, setModalVisibility] = useState(false)
    const [uploadingImage, setUploadingImage] = useState(false);
    const [uploadProgress, setUploadProgress] = useState("");
    const navigate = useNavigate()

    const handleSave = async (event) => {
        event.preventDefault()
        if (!loading && !uploadingImage) {
            setLoading(true)
            if (userProfile.phoneNumber.length !== 10) {
                toast.warn("Please enter a valid Phone Number")
                return
            }
            
            await set(ref(firebaseDatabase, "STUDENTS_ARCHIVE/"+userData.department+"/"+userData.uid), userProfile)
            if (userProfile.accountType === "LINKED") {
                await set(ref(firebaseDatabase, "COLLEGE_WISE_STUDENTS/"+userData.collegeUid+"/"+userData.department+"/"+userData.collegeKey), userProfile)
            }
            setUserData(userProfile)
            toast.success("Profile updated")
            setLoading(false);
        }
    }

    const handleForgot = async () => {
        setModalVisibility(false)
        try {
            await sendPasswordResetEmail(firebaseAuth, userData.emailId)
            alert("We have sent a reset-password link to your email-Id. Please click on the link to reset your password and login again.")
            firebaseAuth.signOut()
        } catch (e) {
            alert("Something went wrong. Please confirm your email-Id or try again later.")
        }
    }

    const handleImage = () => {
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type',"file");
        inputElement.setAttribute('accept','image/*');
        inputElement.onchange = (event) => {
            const files = event.target.files;
            const logoRef = firebaseStorageRef(firebaseStorage, `STUDENT_PICTURE/`+userData.uid);
            const uploadTask = uploadBytesResumable(logoRef,files[0], {contentType:'image/png'}); 
            uploadTask.on('state_changed', (snapshot) => {
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                setUploadProgress(progress.toFixed(2).toString());
                setUploadingImage(true);
            }, () => {
                alert("Something went wrong.")
                setUploadingImage(false);
            }, async () => {
                const url = await getDownloadURL(logoRef);
                setUserProfile({...userProfile, picture:url});
                await set(ref(firebaseDatabase, "STUDENTS_ARCHIVE/"+userData.department+"/"+userData.uid+"/picture"), url)
                if (userProfile.accountType === "LINKED") {
                    await set(ref(firebaseDatabase, "COLLEGE_WISE_STUDENTS/"+userData.collegeUid+"/"+userData.department+"/"+userData.collegeKey+"/picture"), url)
                }
                toast.success('Profile picture updated', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                setUploadingImage(false);
                setUploadingImage(false);
                setUploadProgress("");
            })

        }
        inputElement.click();
    }

    return(
        <div className="profile-container">
            
            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>

            <div className="form-button-container">
                <Form className="job-detail-container" onSubmit={handleSave}>
                    <p className="section-title ete-field" style={{marginLeft:'0'}}>Manage profile</p>

                    <div className="ete-field image-container">
                        <Image id="dropdown-basic" roundedCircle className="profile-picture" onClick={handleImage} src={userProfile.picture?userProfile.picture:'https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde'} />
                        <p className="picture-tag" onClick={handleImage}>
                            {
                                uploadProgress
                                ?
                                `${uploadProgress}% Uploaded`
                                :
                                "Click to change picture"
                            }
                        </p>
                    </div>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Name</Form.Label>
                        <Form.Control className="field" required value={userProfile.name} onChange={(event)=>{setUserProfile({...userProfile, name:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Phone Number</Form.Label>
                        <Form.Control className="field" maxLength={10} required value={userProfile.phoneNumber} onChange={(event)=>{setUserProfile({...userProfile, phoneNumber:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Email-id</Form.Label>
                        <Form.Control className="field" value={userProfile.emailId} disabled />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Password</Form.Label>
                        <Form.Control className="field" value={"               "} type="password" disabled />
                        <Form.Text className="error-tag" onClick={()=>{setModalVisibility(true)}}>Reset password?</Form.Text>
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">College</Form.Label>
                        <Form.Control className="field" required value={userProfile.collegeName} disabled={userProfile.accountType==="LINKED"} onChange={(event)=>{setUserProfile({...userProfile, collegeName:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Department</Form.Label>
                        <Form.Control className="field" value={userProfile.department} disabled />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">USN</Form.Label>
                        <Form.Control className="field" required value={userProfile.usn} disabled={userData.accountType==="LINKED"} onChange={(event)=>{setUserProfile({...userProfile, usn:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Are you planning to study abroad?</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group1" type='radio' checked={userProfile.abroadStudy==="Yes"} label={"Yes"} value={"Yes"} onChange={(event)=>{setUserProfile({...userProfile, abroadStudy:event.target.value})}} />   
                            <Form.Check inline name="group1" type='radio' checked={userProfile.abroadStudy==="No"} label={"No"} value={"No"} onChange={(event)=>{setUserProfile({...userProfile, abroadStudy:event.target.value})}} /> 
                        </div>
                    </Form.Group>
                    {
                        userProfile.abroadStudy === "Yes"
                        &&
                        <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                            <Form.Label className="tag">Please specify some countries</Form.Label>
                            <Form.Control className="field" required value={userProfile.abroadCountries} onChange={(event)=>{setUserProfile({...userProfile, abroadCountries:event.target.value})}} />
                        </Form.Group>
                    }

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Are you planning for MBA?</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group2" type='radio' checked={userProfile.mbaPlan==="Yes"} label={"Yes"} value={"Yes"} onChange={(event)=>{setUserProfile({...userProfile, mbaPlan:event.target.value})}} />   
                            <Form.Check inline name="group2" type='radio' checked={userProfile.mbaPlan==="No"} label={"No"} value={"No"} onChange={(event)=>{setUserProfile({...userProfile, mbaPlan:event.target.value})}} /> 
                        </div>
                    </Form.Group>
                    {
                        userProfile.mbaPlan === "Yes"
                        &&
                        <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                            <Form.Label className="tag">From where do you plan to pursue MBA</Form.Label>
                            <div className="ete-field">
                                <Form.Check inline name="group3" type='radio' checked={userProfile.mbaPlace==="India"} label={"India"} value={"India"} onChange={(event)=>{setUserProfile({...userProfile, mbaPlace:event.target.value})}} />   
                                <Form.Check inline name="group3" type='radio' checked={userProfile.mbaPlace==="Abroad"} label={"Abroad"} value={"Abroad"} onChange={(event)=>{setUserProfile({...userProfile, mbaPlace:event.target.value})}} /> 
                            </div>                       
                        </Form.Group>
                    }

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Are you planning for M Tech?</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group4" type='radio' checked={userProfile.mTech==="Yes"} label={"Yes"} value={"Yes"} onChange={(event)=>{setUserProfile({...userProfile, mTech:event.target.value})}} />   
                            <Form.Check inline name="group4" type='radio' checked={userProfile.mTech==="No"} label={"No"} value={"No"} onChange={(event)=>{setUserProfile({...userProfile, mTech:event.target.value})}} /> 
                        </div>
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Are you planning for Government Exams?</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group5" type='radio' checked={userProfile.govExam==="Yes"} label={"Yes"} value={"Yes"} onChange={(event)=>{setUserProfile({...userProfile, govExam:event.target.value})}} />   
                            <Form.Check inline name="group5" type='radio' checked={userProfile.govExam==="No"} label={"No"} value={"No"} onChange={(event)=>{setUserProfile({...userProfile, govExam:event.target.value})}} /> 
                        </div>
                    </Form.Group>
                    {
                        userProfile.govExam === "Yes"
                        &&
                        <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                            <Form.Label className="tag">Please name some exams of interest</Form.Label>
                            <Form.Control className="field" required value={userProfile.govExamName} onChange={(event)=>{setUserProfile({...userProfile, govExamName:event.target.value})}} />
                        </Form.Group>
                    }

                    <Button type="submit" variant="primary" className="ete-field button" >
                        {
                            loading
                            ?
                            <Spinner animation="border" size="sm" />
                            :
                            "Save"
                        }
                    </Button>
                    {/* <div className="ete-field download-container" onClick={()=>{firebaseAuth.signOut()}}>
                        <FiLogOut size={25} color="#3761EE" />
                        <p className="download-tag">Logout</p>
                    </div> */}
                </Form>

                
                
            </div>

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />

            <Modal show={modalVisibility} onHide={()=>{setModalVisibility(false)}}>
                <Modal.Header closeButton >
                  <Modal.Title>Send link?</Modal.Title>
                </Modal.Header>
                                
                <Modal.Body>
                  <p>Are you sure you want a password reset link sent to your email-Id ({userData.emailId})?</p>
                </Modal.Body>
                                
                <Modal.Footer>
                  <Button variant="secondary" onClick={()=>{setModalVisibility(false)}}>Close</Button>
                  <Button variant="primary" onClick={handleForgot}>Send</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default ManageProfile