import React, { useEffect, useState } from "react";
import logo from '../../assets/logo.png'
import { Form, Button, Spinner } from "react-bootstrap";
import "./signup.style.scss"
import userSample from "../../entities/user-sample";
import { ref, set, get, child } from 'firebase/database'
import { firebaseAuth, firebaseDatabase } from "../../backend/firebase-handler";
import { createUserWithEmailAndPassword, getAuth, updateProfile } from 'firebase/auth'
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import validator  from 'validator'
import departmentList from "../../entities/department-list";

const SignupPage = ({setCurrentState}) => {

    const [userProfile, setUserProfile] = useState(userSample)
    const [loading, setLoading] = useState(false)
    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")
    const [currentError, setCurrentError] = useState("")
    const [collegeList, setCollegeList] = useState([])
    const [selectedCollege, setSelectedCollege] = useState({})
    const [gettingList, setGettingList] = useState(true)

    useEffect(() => {
        window.scrollTo(0, 0)

        let temp = []
        get(child(ref(firebaseDatabase), "COLLEGE_ARCHIVE")).then((snap) => {
            if (snap.exists()) {
               
                for (const key in snap.val()) {
                    var item = snap.child(key).val()
                    temp.push(item)
                }
                temp.reverse()
                temp.push({collegeName:"Other", uid:"Other"})
                setCollegeList(temp)
                setSelectedCollege(temp[0])
                setGettingList(false)
            }
        })

    }, [])

    const handleCollegeDD = (event) => {
        
        setSelectedCollege(JSON.parse(event.target.value))
    }

    const handleSignup = async () => {
        if (userProfile.name === "") {
            setCurrentError("name")
            return
        }
        if (userProfile.phoneNumber.length !== 10 || !validator.isMobilePhone(userProfile.phoneNumber)) {
            setCurrentError("phone")
            return
        }
        if (userProfile.emailId === "" || !validator.isEmail(userProfile.emailId)) {
            setCurrentError("email")
            return
        }
        if (password.length < 8) {
            setCurrentError("password")
            return
        }
        if (password !== confirmPassword) {
            setCurrentError("confirmPassword")
            return
        }
        if (selectedCollege.uid==="Other" && userProfile.collegeName === "") {
            setCurrentError("college")
            return
        }
        if (userProfile.usn === "") {
            setCurrentError("usn")
            return
        }
        if (userProfile.department === "") {
            setCurrentError("department")
            return
        }
        if (userProfile.semester === "") {
            setCurrentError("semester")
            return
        }
        if (userProfile.yearOfStudy === "") {
            setCurrentError("year")
            return
        }
        if (userProfile.abroadStudy === "Yes" && userProfile.abroadCountries === "") {
            toast.warn("Please mention some countries of interest for abroad study")
            return
        }
        if (userProfile.govExam === "Yes" && userProfile.govExamName === "") {
            toast.warn("Please mention some government exams of interest")
            return
        }

        setCurrentError("")
        setLoading(true)

        var date = new Date().getDate(); 
        var month = new Date().getMonth() + 1; 
        var year = new Date().getFullYear(); 
        var hours = new Date().getHours(); 
        var min = new Date().getMinutes();
        var sec = new Date().getSeconds(); 
        userProfile.date = date + "-" + month + "-" + year + " " + hours + ":" + min + ":" + sec
        if (selectedCollege.uid !== "Other") {
            userProfile.collegeName = selectedCollege.collegeName
            userProfile.collegeUid = selectedCollege.uid
        }
        createUserWithEmailAndPassword(firebaseAuth, userProfile.emailId, password).then((credential) => {
            userProfile.uid = credential.user.uid
            userProfile.key = credential.user.uid
            set(ref(firebaseDatabase, "STUDENTS_ARCHIVE/"+userProfile.department+"/"+userProfile.uid), userProfile).then((user) => {
                updateProfile(getAuth().currentUser, {
                    displayName:userProfile.department
                })
                toast.success("Account created.")
                setCurrentState("HOME")
                setLoading(false)
            })
            .catch ((error) => {
                toast.error("Something went wrong. Please try with a different email-Id or try again later.")
                setLoading(false)
            })
        })
        .catch((error) => {
            toast.error("Something went wrong. Please try with a different email-Id or try again later.")
            setLoading(false)
        })
    }

    return(
        <div className="signup-page-container">
            <img src={logo} alt="OccuHire" className="logo" />

            {
                gettingList
                ?
                <Spinner animation="border" variant="primary" style={{margin:"auto"}} />
                :
                <div className="signup-form-container">
                    <p className="welcome-tag">Create a new account</p>

                    <Form className="form-container">
                        <div className="field-sub-container">
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1" style={{marginRight:10}}>
                                <Form.Label className="tag">Name</Form.Label>
                                <Form.Control className="field" type="text" placeholder="FirstName LastName" value={userProfile.name} onChange={(event)=>{setUserProfile({...userProfile, name:event.target.value})}} />
                                <Form.Text style={currentError==="name"?{}:{display:"none"}} className="error-tag">Please enter your name</Form.Text>
                            </Form.Group>
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                <Form.Label className="tag">Phone Number</Form.Label>
                                <Form.Control maxLength={10} className="field" type="number-pad" placeholder="+91 xxxxx xxxxx" value={userProfile.phoneNumber} onChange={(event)=>{setUserProfile({...userProfile, phoneNumber:event.target.value.replace(/[^0-9]/g, '')})}} />
                                <Form.Text style={currentError==="phone"?{}:{display:"none"}} className="error-tag">Please enter a valid phone number</Form.Text>
                            </Form.Group>
                        </div>
                        <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                            <Form.Label className="tag">Email address</Form.Label>
                            <Form.Control className="field" type="email" placeholder="c x@example.com" value={userProfile.emailId} onChange={(event)=>{setUserProfile({...userProfile, emailId:event.target.value})}} />
                            <Form.Text style={currentError==="email"?{}:{display:"none"}} className="error-tag">Please enter a valid email-id</Form.Text>
                        </Form.Group>
                        <div className="field-sub-container">
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1" style={{marginRight:10}}>
                                <Form.Label className="tag">Password</Form.Label>
                                <Form.Control className="field" type="password" placeholder="* * * * * * * *" value={password} onChange={(event)=>{setPassword(event.target.value)}} />
                                <Form.Text style={currentError==="password"?{}:{display:"none"}} className="error-tag">Please enter a password with at least 8 characters</Form.Text>
                            </Form.Group>
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                <Form.Label className="tag">Confirm Password</Form.Label>
                                <Form.Control className="field" type="password" placeholder="* * * * * * * *" value={confirmPassword} onChange={(event)=>{setConfirmPassword(event.target.value)}} />
                                <Form.Text style={currentError==="confirmPassword"?{}:{display:"none"}} className="error-tag">Passwords do not match</Form.Text>
                            </Form.Group>
                        </div>
                    
                        <Form.Group className="field-container" controlId="exampleForm.ControlInput1" style={{marginRight:10}}>
                            <Form.Label className="tag">Select College</Form.Label>
                            <Form.Select aria-label="Default select example" onChange={handleCollegeDD}   >
                                {
                                    collegeList.map((item, index)=>{return(
                                        <option className="field" value={JSON.stringify(item)}>{item.collegeName}</option>
                                    )})
                                }
                            </Form.Select>
                        </Form.Group>
                        {
                            selectedCollege.uid === "Other"
                            &&
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                <Form.Label className="tag">College Name</Form.Label>
                                <Form.Control className="field" type="text" placeholder="ABC College" value={userProfile.collegeName} onChange={(event)=>{setUserProfile({...userProfile, collegeName:event.target.value})}} />
                                <Form.Text style={currentError==="college"?{}:{display:"none"}} className="error-tag">Please enter your college name</Form.Text>
                            </Form.Group>
                        }

                        <div className="field-sub-container">
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1" style={{marginRight:10}}>
                                <Form.Label className="tag">Department</Form.Label>
                                <Form.Select aria-label="Default select example" onChange={(event)=>{setUserProfile({...userProfile, department:event.target.value})}}>
                                    {
                                        departmentList.map((item, index)=>{return(
                                            <option className="field" value={item}>{item}</option>
                                        )})
                                    }
                                </Form.Select>
                            </Form.Group>
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                <Form.Label className="tag">USN</Form.Label>
                                <Form.Control className="field" type="text" placeholder="12ABCCS098" value={userProfile.usn} onChange={(event)=>{setUserProfile({...userProfile, usn:event.target.value})}} />
                                <Form.Text style={currentError==="usn"?{}:{display:"none"}} className="error-tag">Please enter your USN</Form.Text>
                            </Form.Group>
                        </div>

                        <div className="field-sub-container">
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1" style={{marginRight:10}}>
                                <Form.Label className="tag">Semester</Form.Label>
                                <Form.Control className="field" type="text" placeholder="4th Semester" value={userProfile.semester} onChange={(event)=>{setUserProfile({...userProfile, semester:event.target.value.replace(/[^0-9]/g, '')})}} />
                                <Form.Text style={currentError==="semester"?{}:{display:"none"}} className="error-tag">Please enter your semester</Form.Text>
                            </Form.Group>
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                <Form.Label className="tag">Year of Graduation</Form.Label>
                                <Form.Control className="field" type="text" placeholder="2022" value={userProfile.yearOfStudy} onChange={(event)=>{setUserProfile({...userProfile, yearOfStudy:event.target.value.replace(/[^0-9]/g, '')})}} />
                                <Form.Text style={currentError==="year"?{}:{display:"none"}} className="error-tag">Please enter your Year of Graduation</Form.Text>
                            </Form.Group>
                        </div>

                        <div className="field-sub-container">
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                <Form.Label className="tag">Are you planning to study abroad?</Form.Label>
                                <div className="ete-field">
                                    <Form.Check inline name="group1" type='radio' checked={userProfile.abroadStudy==="Yes"} label={"Yes"} value={"Yes"} onChange={(event)=>{setUserProfile({...userProfile, abroadStudy:event.target.value})}} />   
                                    <Form.Check inline name="group1" type='radio' checked={userProfile.abroadStudy==="No"} label={"No"} value={"No"} onChange={(event)=>{setUserProfile({...userProfile, abroadStudy:event.target.value})}} /> 
                                </div>
                            </Form.Group>
                            {
                                userProfile.abroadStudy === "Yes"
                                &&
                                <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                    <Form.Label className="tag">Please specify some countries</Form.Label>
                                    <Form.Control className="field" required value={userProfile.abroadCountries} onChange={(event)=>{setUserProfile({...userProfile, abroadCountries:event.target.value})}} />
                                </Form.Group>
                            }
                        </div>

                        <div className="field-sub-container">
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                <Form.Label className="tag">Are you planning for MBA?</Form.Label>
                                <div className="ete-field">
                                    <Form.Check inline name="group2" type='radio' checked={userProfile.mbaPlan==="Yes"} label={"Yes"} value={"Yes"} onChange={(event)=>{setUserProfile({...userProfile, mbaPlan:event.target.value})}} />   
                                    <Form.Check inline name="group2" type='radio' checked={userProfile.mbaPlan==="No"} label={"No"} value={"No"} onChange={(event)=>{setUserProfile({...userProfile, mbaPlan:event.target.value})}} /> 
                                </div>
                            </Form.Group>
                            {
                                userProfile.mbaPlan === "Yes"
                                &&
                                <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                    <Form.Label className="tag">From where do you plan to pursue MBA</Form.Label>
                                    <div className="ete-field">
                                        <Form.Check inline name="group3" type='radio' checked={userProfile.mbaPlace==="India"} label={"India"} value={"India"} onChange={(event)=>{setUserProfile({...userProfile, mbaPlace:event.target.value})}} />   
                                        <Form.Check inline name="group3" type='radio' checked={userProfile.mbaPlace==="Abroad"} label={"Abroad"} value={"Abroad"} onChange={(event)=>{setUserProfile({...userProfile, mbaPlace:event.target.value})}} /> 
                                    </div>                       
                                </Form.Group>
                            }
                        </div>

                        <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                            <Form.Label className="tag">Are you planning for M Tech?</Form.Label>
                            <div className="ete-field">
                                <Form.Check inline name="group4" type='radio' checked={userProfile.mTech==="Yes"} label={"Yes"} value={"Yes"} onChange={(event)=>{setUserProfile({...userProfile, mTech:event.target.value})}} />   
                                <Form.Check inline name="group4" type='radio' checked={userProfile.mTech==="No"} label={"No"} value={"No"} onChange={(event)=>{setUserProfile({...userProfile, mTech:event.target.value})}} /> 
                            </div>
                        </Form.Group>

                        <div className="field-sub-container">
                            <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                <Form.Label className="tag">Are you planning for Government Exams?</Form.Label>
                                <div className="ete-field">
                                    <Form.Check inline name="group5" type='radio' checked={userProfile.govExam==="Yes"} label={"Yes"} value={"Yes"} onChange={(event)=>{setUserProfile({...userProfile, govExam:event.target.value})}} />   
                                    <Form.Check inline name="group5" type='radio' checked={userProfile.govExam==="No"} label={"No"} value={"No"} onChange={(event)=>{setUserProfile({...userProfile, govExam:event.target.value})}} /> 
                                </div>
                            </Form.Group>
                            {
                                userProfile.govExam === "Yes"
                                &&
                                <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                                    <Form.Label className="tag">Please name some exams of interest</Form.Label>
                                    <Form.Control className="field" required value={userProfile.govExamName} onChange={(event)=>{setUserProfile({...userProfile, govExamName:event.target.value})}} />
                                </Form.Group>
                            }
                        </div>

                    </Form>

                    <Button variant="primary" className="login-button" onClick={handleSignup}>
                        {
                            loading
                            ?
                            <Spinner animation="border" variant="light" />
                            :
                            "Create Account"
                        }
                    </Button>
                </div>
            }

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />
        </div>
    )
}

export default SignupPage