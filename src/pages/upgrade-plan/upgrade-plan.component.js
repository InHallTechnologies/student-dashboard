import React from 'react';
import './upgrade-plan.style.scss';
import logo from '../../assets/logo.png';
import { Button, Card, Spinner } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { firebaseAuth, firebaseDatabase } from '../../backend/firebase-handler';
import { ref, get, push, set } from 'firebase/database';
import axios from "axios";
import { useContext } from 'react';
import context from '../../context/app-context';
import { ToastContainer, toast } from 'react-toastify';
import free from '../../assets/free_plan.png'
import basic from '../../assets/basic_plan.png'
import premium from '../../assets/premium_plan.png'
import check from '../../assets/check.png'
import cross from '../../assets/cross.png'
import { IoMdArrowRoundBack } from 'react-icons/io'
import { useNavigate } from 'react-router-dom';

const UpgradePlan = () => {
    const [plans, setPlans] = useState([]);
    const [loading, setLoading] = useState(true);
    const [userData, setUserData] = useContext(context);
    const firebaseUser = firebaseAuth.currentUser;
    const navigate = useNavigate()
    
    console.log(userData);
    useEffect(() => {
        const plansRef = ref(firebaseDatabase, `STUDENT_PLAN_DETAILS`);
        get(plansRef).then(snapshot => {
            if (snapshot.exists()){
                const data = [];
                for (const key in snapshot.val()) {
                    const plan = snapshot.child(key).val();
                    data.push(plan);
                }
                setPlans(data);
                setLoading(false);
            }
        })

    }, [])

    const handleVerification = (type) => {
        switch (type) {
            case "FREE": {
                if (userData.plan === "Free Plan") {
                    toast.warn("This is the plan you are using currently")
                } else {
                    toast.warn("Sorry, you can not downgrade your plan.")
                }
                break
            }
            case "BASIC": {
                if (userData.plan === "Free Plan") {
                    handlePaymentClicks(plans[0])
                } else if (userData.plan === "Basic Plan"){
                    toast.warn("This is the plan you are using currently")
                } else {
                    toast.warn("Sorry, you can not downgrade your plan.")
                }
                break
            }
            case "PREMIUM": {
                if (userData.plan === "Free Plan") {
                    handlePaymentClicks(plans[2])
                } else if (userData.plan === "Basic Plan"){
                    handlePaymentClicks(plans[2])
                } else {
                    toast.warn("This is the plan you are using currently")
                }
                break
            }
            default: {}
        }
    }

    const handlePaymentClicks = async (item) => {
        const { price, name } = item;
        const studentRef = ref(firebaseDatabase, `STUDENTS_ARCHIVE`);
        const orderId = push(studentRef).key;
        const requestUrl = `https://us-central1-occuhire-tequed-labs.cloudfunctions.net/getOrder?amount=${parseInt(price) * 100}&receipt=${orderId}`
        const response = await axios.get(requestUrl);
        const resposeData = response.data;
        const {id} = resposeData;

        var options = {
            "key": "rzp_test_kXLAp5bAToWomX", // Enter the Key ID generated from the Dashboard
            "amount": `${parseInt(price) * 100}`, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
            "currency": "INR",
            "name": "OccuHire",
            "description": `Plan upgrade to ${name}`,
            "image": "https://example.com/your_logo",
            "order_id": `${id}`, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
            "handler": async function (response){
                const plansRef = ref(firebaseDatabase, `STUDENTS_ARCHIVE/${userData.department}/${userData.key}/plan`);
                await set(plansRef, name);
                toast.success(`Successfully upgraded plan to ${name}`, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    });
            },
            "prefill": {
                "name": "Gaurav Kumar",
                "email": "gaurav.kumar@example.com",
                "contact": "9999999999"
            },
            "notes": {
                "address": "Razorpay Corporate Office"
            },
            "theme": {
                "color": "#3399cc"
            }
        };
        var rzp1 = new window.Razorpay(options);
        rzp1.on('payment.failed', function (response){
            alert(response.error.code);
            alert(response.error.description);
            alert(response.error.source);
            alert(response.error.step);
            alert(response.error.reason);
            alert(response.error.metadata.order_id);
            alert(response.error.metadata.payment_id);
        });
        rzp1.open();
    }

    if (loading) {
        return (
            <div style={{width:'100%', height:'100vh', display:'flex', justifyContent:'center', alignItems:'center'}}>
                <Spinner animation="border" />
            </div>
        )
    }

    return(
        <div className='upgrade-plan-container'>

            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>

            <p className='title'>Plans & Prices</p>
            <p className='sub-title'>Choose the best fitting plan and get unique features for your account</p>
            
            <div className='plans-list'>
                <div className='plan-container'>
                    <img src={free} className='plan-image' alt='free-plan' />
                    <p className='plan-name'>Free Plan</p>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>20 Job Options</p>
                    </div>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>Package 2L - 4L</p>
                    </div>
                    <div className='feature-container'>
                        <img src={cross} alt='check' className='check' />
                        <p className='feature-tag'>Mentoring Support</p>
                    </div>
                    <div className='feature-container'>
                        <img src={cross} alt='check' className='check' />
                        <p className='feature-tag'>Resume Booster</p>
                    </div>
                    <div className='feature-container'>
                        <img src={cross} alt='check' className='check' />
                        <p className='feature-tag'>Skill Enhancement Courses</p>
                    </div>
                    <div className='feature-container'>
                        <img src={cross} alt='check' className='check' />
                        <p className='feature-tag'>AI Based Job Recommendations</p>
                    </div>
                    <Button variant='primary' className='submit-button' onClick={()=>{handleVerification("FREE")}}>Get for free</Button>
                </div>
                <div className='plan-container'>
                    <img src={basic} className='plan-image' alt='free-plan' />
                    <p className='plan-name'>Basic Plan</p>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>50 Job Options</p>
                    </div>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>Package 2L - 7.5L</p>
                    </div>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>Mentoring Support</p>
                    </div>
                    <div className='feature-container'>
                        <img src={cross} alt='check' className='check' />
                        <p className='feature-tag'>Resume Booster</p>
                    </div>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>Aptitude + Soft Skills</p>
                    </div>
                    <div className='feature-container'>
                        <img src={cross} alt='check' className='check' />
                        <p className='feature-tag'>AI Based Job Recommendations</p>
                    </div>
                    <Button variant='primary' className='submit-button' onClick={()=>{handleVerification("BASIC")}}>Get for ₹500</Button>
                </div>
                <div className='plan-container'>
                    <img src={premium} className='plan-image' alt='free-plan' />
                    <p className='plan-name'>Premium Plan</p>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>Unlimited Job Options</p>
                    </div>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>All Packages</p>
                    </div>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>Mentoring Support</p>
                    </div>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>Resume Booster</p>
                    </div>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>Aptitude + Soft Skills</p>
                    </div>
                    <div className='feature-container'>
                        <img src={check} alt='check' className='check' />
                        <p className='feature-tag'>AI Based Job Recommendations</p>
                    </div>
                    <Button variant='primary' className='submit-button' onClick={()=>{handleVerification("PREMIUM")}}>Get for ₹2,000</Button>
                </div>
                {/* <Card key={item.name} >
                    <Card.Body>
                        <Card.Title>{item.name}</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">₹ {item.price}</Card.Subtitle>
                        <Card.Text>
                            Apply for {item.numberOfOptions} offers
                        </Card.Text>
                        {
                            item.price === "0"
                            ?
                            <Card.Link>Free</Card.Link>
                            :
                            <Card.Link onClick={_ => handlePaymentClicks(item)}>Buy Plan</Card.Link>
                        }
                        
                    </Card.Body>
                </Card> */}
            </div>
            <ToastContainer />
        </div>
    )
}

export default UpgradePlan;