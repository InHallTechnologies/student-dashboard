import React, { useEffect, useState } from "react";
import './login.style.scss'
import logo from '../../assets/logo.png'
import { Form, Spinner, Button, Image } from "react-bootstrap";
import { signInWithEmailAndPassword, sendPasswordResetEmail } from 'firebase/auth'
import { firebaseAuth, firebaseDatabase } from "../../backend/firebase-handler";
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import welcome from '../../assets/welcome-login-page.png';
import feature from '../../assets/connect_with.png';
import HowToLandJob from "../../components/how-to-land-job/HowToLandJob.component";
import PlacementStats from "../../components/placement-stats/PlacementStats.component";
import SimpleGridWithTitle from "../../components/simple-grid-with-title/SimpleGridWithTitle.component";
import { get, ref } from "firebase/database";
import FooterContainer from "../../components/footer/Footer.component";

const LoginPage = ({setCurrentState}) => {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [currentError, setCurrentError] = useState("")
    const [loading, setLoading] = useState(false)
    const [recruiters, setRecruiters] = useState([]);
    const [testimonials, setTestimonials] = useState([])

    useEffect(() => {
        const getRecruiters = async () => {
            const recruitersRef = ref(firebaseDatabase, `LOGO_ARCHIVE/RECRUITER`);
            const recruitersSnapshot = await get(recruitersRef);
            if (recruitersSnapshot.exists()) {
                const logos = await recruitersSnapshot.val();
                setRecruiters(logos);
            } 
        }
        getRecruiters();

        const getTestimonials = async () => {
            const testimonialRef = ref(firebaseDatabase, `CAROUSELS/LANDING_PAGE`);
            const testimonialSnapshot = await get(testimonialRef);
            if (testimonialSnapshot.exists()) {
                const testimonial = await testimonialSnapshot.val();
                setTestimonials(testimonial);
            } 
        }
        getTestimonials();
    }, [])

    const handleLogin = () => {
        if (email === "") {
            setCurrentError("email")
            return
        }
        if (password === "") {
            setCurrentError("password")
            return
        }
        setCurrentError("")
        setLoading(true)
        
        signInWithEmailAndPassword(firebaseAuth, email, password).then((credential) => {
            toast.success("Login successful.")
            setLoading(false)
            setCurrentState("HOME")
        })
        .catch((error) => {
            toast.warn("Invalid credentials.")
            setLoading(false)
        })
    }

    const handleForgot = async () => {
        if (email === "") {
            setCurrentError("email")
            return
        } else {
            try {
                await sendPasswordResetEmail(firebaseAuth, email)
                alert("We have sent a reset-password link to your email-Id. Please click on the link to reset your password.")
            } catch (e) {
                alert("Something went wrong. Please confirm your email-Id or try again later.")
            }
        }
    }

    return(
        <div className="login-page-container">
            <div className="login-page-content">
                <img src={logo} alt="OccuHire" className="logo" />
            </div>
            
            <div className="login-hero-container" >

                <div className="illustration-container">
                    <img className='welcome-illustration' src={welcome} />
                </div>

                <div className="login-form-container">
                <p className="welcome-tag">Welcome to OccuHire Student</p>
                <p className="sub-tag">Please login to continue</p>

                <Form className="form-container">
                    <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Email address</Form.Label>
                        <Form.Control className="field" type="email" placeholder="name@example.com" value={email} onChange={(event)=>{setEmail(event.target.value)}} />
                        <Form.Text style={currentError==="email"?{}:{display:"none"}} className="error-tag">Please enter your email-id</Form.Text>
                    </Form.Group>
                    <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Password</Form.Label>
                        <Form.Control className="field" type="email" placeholder="* * * * * * * *" value={password} onChange={(event)=>{setPassword(event.target.value)}} />
                        <Form.Text style={currentError==="password"?{}:{display:"none"}} className="error-tag">Please enter a password with at least 8 characters</Form.Text>
                    </Form.Group>
                </Form>

                <div className="buttons-container">
                    <Button variant="primary" disabled={loading} className="login-button" onClick={handleLogin}>
                        {
                            loading
                            ?
                            <Spinner animation="border" variant="light" />
                            :
                            "Login"
                        }
                    </Button>
                    <p className="secondary-button" onClick={handleForgot}>Forgot Password?</p>
                </div>

                <div className="bar"></div>

                <div className="signup-container">
                    <p className="question">Do not have an account?</p>
                    <p className="secondary-button" onClick={()=>{setCurrentState("SIGNUP")}}>Create a new account</p>
                </div>
                </div>

            </div>
            
            <div className="login-feature-container">
                <Image className='feature-image' src={feature} />
            </div>

            <HowToLandJob />
            <div className="with-pattern-background" >
                <PlacementStats title={"Student Testimonials"} list={testimonials} />
                <SimpleGridWithTitle title={"Our Top Recruiters"} list={recruiters} />
            </div>
            <FooterContainer />

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />
        </div>
    )
}

export default LoginPage