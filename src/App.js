import './App.css';
import { Route, Routes } from 'react-router';
import LoginPage from './pages/login/login.component';
import { useEffect, useState, useContext } from 'react';
import SignupPage from './pages/signup/signup.component';
import HomePage from './pages/home/home.component';
import { firebaseAuth, firebaseDatabase } from './backend/firebase-handler';
import context, { Provider } from './context/app-context';
import { get, ref, child, push, set } from 'firebase/database'
import TestPage from './pages/test/test.component';
import ApplyPage from './pages/apply/apply.component';
import UpgradePlan from './pages/upgrade-plan/upgrade-plan.component';
import ManageProfile from './pages/manage-profile/manage-profile.component';
import TestCompletion from './pages/test-completion/test-completion.component';

function App() {

  const [currentState, setCurrentState] = useState("SPLASH")
  const [userData, setUserData] = useContext(context)

  useEffect(() => {
    // firebaseAuth.signOut()
    firebaseAuth.onAuthStateChanged((user) => {
      if (user) {
        get(child(ref(firebaseDatabase), "STUDENTS_ARCHIVE/"+user.displayName+"/"+user.uid)).then((dataSnapshot) => {
          if (dataSnapshot.exists()) {
            setUserData(dataSnapshot.val())
            setCurrentState("HOME")
          } else {
            setCurrentState("LOGIN")
          }
        })
      } else {
        setCurrentState("LOGIN")
      }
    })
  }, [])

  if (currentState === "SPLASH") {
    return(
      <div></div>
    )
  }

  if (currentState === "LOGIN") {
    return(
      <LoginPage setCurrentState={setCurrentState} />
    )
  }

  if (currentState === "SIGNUP") {
    return(
      <SignupPage setCurrentState={setCurrentState} />
    )
  }

  return (
    <Routes>
      <Route path="/" element={<HomePage setCurrentState={setCurrentState} />} />
      <Route path="/test" element={<TestPage />} />
      <Route path="/apply" element={<ApplyPage />} />
      <Route path="/upgrade-plan" element={<UpgradePlan />} />
      <Route path="/manage-profile" element={<ManageProfile />} />
      <Route path='/comp' element={<TestCompletion/>} />
    </Routes>
  );
}

export default () => {
  return (
    <Provider>
      <App/>
    </Provider>
  )
};
